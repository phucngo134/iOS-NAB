//
//  UnitOptionViewModelTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 18/04/2021.
//

import XCTest
@testable import iOS_NAB

class UnitOptionViewModelTests: XCTestCase {

    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func test_unitOptionViewModels_withSameContent_shouldBeEqual() {
        // Arrange
        let vm1 = OptionViewModel(name: TempUnit.standard.rawValue, isSelected: false)
        let vm2 = OptionViewModel(name: TempUnit.standard.rawValue, isSelected: false)
        
        let vm3 = OptionViewModel(name: TempUnit.imperial.rawValue, isSelected: true)
        let vm4 = OptionViewModel(name: TempUnit.imperial.rawValue, isSelected: true)
        
        // Act & Assert
        XCTAssertEqual(vm1, vm2)
        XCTAssertTrue(vm1.isContentEqual(to: vm2))
        
        XCTAssertEqual(vm3, vm4)
        XCTAssertTrue(vm3.isContentEqual(to: vm4))
    }
    
    func test_unitOptionViewModels_withDifferentContent_shouldBeDifferent() {
        // Arrange
        let vm1 = OptionViewModel(name: TempUnit.standard.rawValue, isSelected: false)
        let vm2 = OptionViewModel(name: TempUnit.metric.rawValue, isSelected: false)
        
        let vm3 = OptionViewModel(name: TempUnit.metric.rawValue, isSelected: false)
        let vm4 = OptionViewModel(name: TempUnit.metric.rawValue, isSelected: true)
        
        // Act & Assert
        XCTAssertNotEqual(vm1, vm2)
        XCTAssertFalse(vm1.isContentEqual(to: vm2))
        
        XCTAssertNotEqual(vm3, vm4)
        XCTAssertFalse(vm3.isContentEqual(to: vm4))
    }
}
