//
//  UnitOptionsInteractorTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 18/04/2021.
//

import XCTest
@testable import iOS_NAB

class UnitOptionsInteractorTests: XCTestCase {

    private var sut: UnitOptionsInteractor!
    
    private let settings = SettingsMock()
    
    override func setUpWithError() throws {
        sut = UnitOptionsInteractor(settings: settings)
    }

    override func tearDownWithError() throws {
        settings.reset()
    }

    func test_lastSelectedTempUnit_withNoPreviousValue_shouldReturnDefaultValue() throws {
        // Arrange
        
        // Act
        let selectedTempUnit = sut.lastSelectedTempUnit
        
        // Assert
        XCTAssertEqual(selectedTempUnit, UnitOptionsInteractor.Defaults.tempUnit)
    }
    
    func test_lastSelectedTempUnit_withPreviousValue_shouldReturnPreviousValue() throws {
        // Arrange
        settings.saveString(key: .tempUnit, value: TempUnit.imperial.rawValue)
        
        // Act
        let selectedTempUnit = sut.lastSelectedTempUnit
        
        // Assert
        XCTAssertEqual(selectedTempUnit, .imperial)
    }
}

private class SettingsMock: StringSettingsStoring {
    var defaults: [String: Any] = [:]
    
    func saveString(key: SettingKeys, value: String) {
        guard key == .tempUnit else { return }
        defaults[key.rawValue] = value
    }
    
    func loadString(key: SettingKeys) -> String? {
        guard key == .tempUnit else { return nil }
        return defaults[key.rawValue] as? String
    }
    
    func remove(key: SettingKeys) {
        defaults[key.rawValue] = nil
    }
    
    func reset() {
        defaults.removeAll()
    }
}
