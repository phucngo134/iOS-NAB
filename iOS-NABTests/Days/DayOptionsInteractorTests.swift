//
//  DayOptionsInteractorTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 18/04/2021.
//

import XCTest
@testable import iOS_NAB

class DayOptionsInteractorTests: XCTestCase {

    private var sut: DayOptionsInteractor!
    
    private let settings = SettingsMock()
    
    override func setUpWithError() throws {
        sut = DayOptionsInteractor(settings: settings)
    }

    override func tearDownWithError() throws {
        settings.reset()
    }

    func test_lastSelectedDays_withNoPreviousValue_shouldReturnDefaultValue() throws {
        // Arrange
        
        // Act
        let selectedDays = sut.lastSelectedDays
        
        // Assert
        XCTAssertEqual(selectedDays, DayOptionsInteractor.Defaults.days)
    }
    
    func test_lastSelectedDays_withPreviousValue_shouldReturnPreviousValue() throws {
        // Arrange
        settings.saveInt(key: .days, value: 12)
        
        // Act
        let selectedDays = sut.lastSelectedDays
        
        // Assert
        XCTAssertEqual(selectedDays, 12)
    }
}

private class SettingsMock: IntSettingsStoring {
    var defaults: [String: Any] = [:]
    
    func saveInt(key: SettingKeys, value: Int) {
        guard key == .days else { return }
        defaults[key.rawValue] = value
    }
    
    func loadInt(key: SettingKeys) -> Int {
        guard key == .days else { return 0 }
        return defaults[key.rawValue] as? Int ?? 0
    }
    
    func remove(key: SettingKeys) {
        defaults[key.rawValue] = nil
    }
    
    func reset() {
        defaults.removeAll()
    }
}
