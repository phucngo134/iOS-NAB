//
//  DayOptionsPresenterTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 18/04/2021.
//

import XCTest
import DifferenceKit
@testable import iOS_NAB

class DayOptionsPresenterTests: XCTestCase {

    private var sut: DayOptionsPresenter!
    
    private let interactor = DayOptionsInteractorMock()
    private let router = DayOptionsRouterMock()
    private let view = DayOptionsViewMock()
    private let cell = OptionCellMock()
    
    override func setUpWithError() throws {
        sut = DayOptionsPresenter(interactor: interactor, router: router)
    }

    override func tearDownWithError() throws {
        interactor.reset()
        router.reset()
        view.reset()
        cell.reset()
    }

    func test_totalItems_withInternalViewModels_shouldReturnCorrectCount() throws {
        // Arrange
        populateInternalViewModels()
        
        // Act
        let totalItems = sut.totalItems
        
        // Assert
        XCTAssertEqual(totalItems, Defaults.viewModels.count)
    }

    func test_totalItems_withEmptyViewModels_shouldReturnZero() throws {
        // Arrange
        
        // Act
        let totalItems = sut.totalItems
        
        // Assert
        XCTAssertEqual(totalItems, 0)
    }
    
    func test_prepareData() {
        // Arrange
        sut.view = view
        
        // Act
        sut.prepareData()
        
        // Assert
        XCTAssertEqual(view.reloadChangesWasCalled, true)
        XCTAssertEqual(view.changes?.isEmpty, false)
    }
    
    func test_displayOption_withCorrectIndex() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        sut.displayOption(cell, atIndex: 0)
        
        // Assert
        XCTAssertEqual(cell.setupOptionWasCalled, true)
    }
    
    func test_displayOption_withIncorrectIndex() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        sut.displayOption(cell, atIndex: 2)
        
        // Assert
        XCTAssertEqual(cell.setupOptionWasCalled, false)
    }
    
    func test_saveSelectedDays() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        sut.saveSelectedDays(atIndex: 0)
        
        // Assert
        XCTAssertEqual(interactor.lastSelectedDays, Int(Defaults.viewModels[0].name) ?? 0)
    }
    
    func test_popViewController() {
        // Arrange
        
        // Act
        sut.popViewController()
        
        // Assert
        XCTAssertEqual(router.popViewControllerWasCalled, true)
    }
}

private extension DayOptionsPresenterTests {
    
    enum Defaults {
        static let viewModels: [OptionViewModel] = [
            OptionViewModel(name: "1", isSelected: false),
            OptionViewModel(name: "2", isSelected: false)
        ]
    }
    
    func populateInternalViewModels() {
        sut.syncData(Defaults.viewModels)
    }
}

private class DayOptionsViewMock: UIViewController, DayOptionsViewProtocol {
    var onChangeData: OnChangeDataAction?
    var reloadChangesWasCalled: Bool = false
    var updateUIWasCalled: Bool = false
    var changes: StagedChangeset<[OptionViewModel]>?
    
    func reload(changes: StagedChangeset<[OptionViewModel]>) {
        reloadChangesWasCalled = true
        self.changes = changes
    }
    
    func updateUI() {
        updateUIWasCalled = true
    }
    
    func reset() {
        reloadChangesWasCalled = false
        updateUIWasCalled = false
    }
}

private class DayOptionsInteractorMock: DayOptionsInteraction {
    var lastSelectedDays: Int = 0
    
    func reset() {
        lastSelectedDays = 7
    }
}

private class DayOptionsRouterMock: RouterProtocol {
    var viewController: UIViewController?
    var popViewControllerWasCalled: Bool = false
    
    func popViewController() {
        popViewControllerWasCalled = true
    }
    
    func reset() {
        popViewControllerWasCalled = false
    }
}

private class OptionCellMock: OptionCellDisplay {
    static var reuseIdentifier: String = "OptionCellMock"
    var setupOptionWasCalled: Bool = false
    
    func setupOption(_ viewModel: OptionViewModel) {
        setupOptionWasCalled = true
    }
   
    func reset() {
        setupOptionWasCalled = false
    }
}
