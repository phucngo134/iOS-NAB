//
//  SearchCityHistoryModelTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 17/04/2021.
//

import XCTest
@testable import iOS_NAB

class SearchCityHistoryModelTests: XCTestCase {

    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func test_searchHistoryModel_withSameContent_shouldBeEqual() throws {
        // Arrange
        let createdAt = Date()
        let searchHistory1 = SearchCityHistory(city: Defaults.cities[0], createdAt: createdAt)
        let searchHistory2 = SearchCityHistory(city: Defaults.cities[0], createdAt: createdAt)
        
        // Act & Assert
        XCTAssertEqual(searchHistory1, searchHistory2)
    }
    
    func test_searchHistoryModel_withDifferentContent_shouldBeDifferent() throws {
        // Arrange
        let createdAt = Date()
        let searchHistory1 = SearchCityHistory(city: Defaults.cities[0], createdAt: createdAt)
        let searchHistory2 = SearchCityHistory(city: Defaults.cities[1], createdAt: createdAt)
        
        let createdAt2 = Date().addingTimeInterval(600)
        let searchHistory3 = SearchCityHistory(city: Defaults.cities[1], createdAt: createdAt)
        let searchHistory4 = SearchCityHistory(city: Defaults.cities[1], createdAt: createdAt2)
        
        // Act & Assert
        XCTAssertNotEqual(searchHistory1, searchHistory2)
        XCTAssertNotEqual(searchHistory3, searchHistory4)
    }
}

private enum Defaults {
    static let cities = [
        City(id: 3121766, name: "Galapagar", country: "Spain"),
        City(id: 3121751, name: "Galdakao", country: "Spain")
    ]
}
