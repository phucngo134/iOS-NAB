//
//  CityModelTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 17/04/2021.
//

import XCTest
@testable import iOS_NAB

class CityModelTests: XCTestCase {

    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func test_cityModel_withSameContent_shouldBeEqual() throws {
        // Arrange
        let city1 = City(id: 3121766, name: "Galapagar", country: "Spain")
        let city2 = City(id: 3121766, name: "Galapagar", country: "Spain")
        
        // Act & Assert
        XCTAssertEqual(city1, city2)
    }
    
    func test_cityModel_withDifferentContent_shouldBeDifferent() throws {
        // Arrange
        let city1 = City(id: 3121766, name: "Galapagar", country: "Spain")
        let city2 = City(id: 3121751, name: "Galdakao", country: "Spain")
        
        // Act & Assert
        XCTAssertNotEqual(city1, city2)
    }
}
