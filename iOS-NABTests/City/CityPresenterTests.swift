//
//  CityPresenterTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 17/04/2021.
//

import XCTest
import DifferenceKit
@testable import iOS_NAB

class CityPresenterTests: XCTestCase {

    private var sut: CityPresenter!
    private let interactor = CityInteractorMock()
    private let view = CityViewMock()
    private let cityCell = CityCellMock()
    
    override func setUpWithError() throws {
        sut = CityPresenter(interactor: interactor)
    }

    override func tearDownWithError() throws {
        interactor.reset()
        view.reset()
        cityCell.reset()
    }

    func test_totalItems() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        let totalItems = sut.totalItems
        
        // Assert
        XCTAssertEqual(totalItems, Defaults.cities.count)
    }
    
    func test_displayCities_withEmptyKeywords() {
        // Arrange
        sut.view = view
        
        // Act
        sut.displayCities(keyword: "")
        
        // Assert
        XCTAssertEqual(interactor.getSearchedCityWasCalled, true)
        XCTAssertEqual(view.reloadChangesWasCalled, true)
    }
    
    func test_displayCities_withSameKeywordsForPreviousDisplayedCities() {
        // Arrange
        sut.view = view
        populateInternalViewModels()
        
        // Act
        sut.displayCities(keyword: "GAL")
        
        // Assert
        XCTAssertEqual(view.reloadChangesWasCalled, false)
        XCTAssertEqual(view.hideAnyStateWasCalled, true)
    }
    
    func test_displayCities_withKeywordForNonexistedCities() {
        // Arrange
        sut.view = view
        
        // Act
        sut.displayCities(keyword: "ABC")
        
        // Assert
        XCTAssertEqual(view.reloadChangesWasCalled, false)
        XCTAssertEqual(view.displayNoCityFoundStateWasCalled, true)
    }
    
    func test_displayCities_withKeywordForExistedCities() {
        // Arrange
        sut.view = view
        
        // Act
        sut.displayCities(keyword: "GAL")
        
        // Assert
        XCTAssertEqual(view.reloadChangesWasCalled, true)
    }
    
    func test_displayCity_withIncorrectIndex() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        sut.displayCity(cityCell, atIndex: 2)
        
        // Assert
        XCTAssertEqual(cityCell.setupCityWasCalled, false)
    }
    
    func test_displayCity_withCorrectIndex() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        sut.displayCity(cityCell, atIndex: 0)
        
        // Assert
        XCTAssertEqual(cityCell.setupCityWasCalled, true)
    }
    
    func test_cityAtIndex_withIncorrectIndex_shouldReturnEmpty() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        let viewModel = sut.city(atIndex: 2)
        
        // Assert
        XCTAssertNil(viewModel)
    }
    
    func test_cityAtIndex_withCorrectIndex_shouldReturnOneViewModel() {
        // Arrange
        populateInternalViewModels()
        let expectedViewModel = CityViewModel(
            city: City(id: 3121766, name: "Galapagar", country: "Spain"),
            isSearchHistory: false
        )
        
        // Act
        let viewModel = sut.city(atIndex: 0)
        
        // Assert
        XCTAssertEqual(viewModel, expectedViewModel)
    }
    
    func test_displaySearchHistory() {
        // Arrange
        sut.view = view
        
        // Act
        sut.displaySearchHistory()
        
        // Assert
        XCTAssertEqual(interactor.getSearchedCityWasCalled, true)
        XCTAssertEqual(view.reloadChangesWasCalled, true)
    }
    
    func test_saveSearchHistory_withCorrectIndex() throws {
        // Arrange
        populateInternalViewModels()
        
        // Act
        sut.saveSearchHistory(atIndex: 0)
        
        // Assert
        XCTAssertEqual(interactor.saveSearchHistoryWasCalled, true)
    }
    
    func test_saveSearchHistory_withIncorrectIndex() throws {
        // Arrange
        populateInternalViewModels()
        
        // Act
        sut.saveSearchHistory(atIndex: 2)
        
        // Assert
        XCTAssertEqual(interactor.saveSearchHistoryWasCalled, false)
    }
}

private extension CityPresenterTests {
    
    func populateInternalViewModels() {
        let viewModels = Defaults.cities.map { CityViewModel(city: $0, isSearchHistory: false) }
        sut.syncData(cities: viewModels)
    }
}

private enum Defaults {
    static let cities = [
        City(id: 3121766, name: "Galapagar", country: "Spain"),
        City(id: 3121751, name: "Galdakao", country: "Spain")
    ]
}

private class CityInteractorMock: CityInteraction {
    
    var saveSearchHistoryWasCalled: Bool = false
    var getSearchedCityWasCalled: Bool = false
    
    func getCities(keyword: String) -> [City] {
        if keyword == "GAL" {
            return [
                City(id: 3121766, name: "Galapagar", country: "Spain"),
                City(id: 3121751, name: "Galdakao", country: "Spain")
            ]
        }
        return []
    }
    
    func getSearchedCity() -> [City] {
        getSearchedCityWasCalled = true
        return [
            City(id: 3121766, name: "Galapagar", country: "Spain"),
            City(id: 3121751, name: "Galdakao", country: "Spain")
        ]
    }
    
    func saveSearchHistory(cityId: Int) {
        saveSearchHistoryWasCalled = true
    }
    
    func reset() {
        saveSearchHistoryWasCalled = false
        getSearchedCityWasCalled = false
    }
}

private class CityCellMock: CityCellDisplay {
    static var reuseIdentifier: String = "CityCellMock"
    var setupCityWasCalled: Bool = false
    
    func setupCity(_ viewModel: CityViewModel) {
        setupCityWasCalled = true
    }
    
    func reset() {
        setupCityWasCalled = false
    }
}

private class CityViewMock: UIViewController, CityViewProtocol {
    
    var displayNoCityFoundStateWasCalled: Bool = false
    var hideAnyStateWasCalled: Bool = false
    var reloadChangesWasCalled: Bool = false
    var onSelectCity: ((CityViewModel) -> Void)?
    
    func displayNoCityFoundState() {
        displayNoCityFoundStateWasCalled = true
    }
    
    func hideAnyState() {
        hideAnyStateWasCalled = true
    }
    
    func reload(changes: StagedChangeset<[CityViewModel]>) {
        reloadChangesWasCalled = true
    }
    
    func reset() {
        displayNoCityFoundStateWasCalled = false
        hideAnyStateWasCalled = false
        reloadChangesWasCalled = false
    }
}
