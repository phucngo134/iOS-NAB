//
//  CityInteractorTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 16/04/2021.
//

import XCTest
import RealmSwift
@testable import iOS_NAB

class CityInteractorTests: XCTestCase {

    private var sut: CityInteractor!
    
    private lazy var realmStorage: RealmStorageContext = {
        let configuration = Realm.Configuration(inMemoryIdentifier: "CityInteractorTests")
        return RealmStorageContext(configuration: configuration)
    }()
    
    override func setUpWithError() throws {
        sut = CityInteractor(storage: realmStorage)
    }

    override func tearDownWithError() throws {
        try realmStorage.reset()
    }

    func test_getCities_withFullNameIrun_shouldReturn1City() throws {
        // Arrange
        try populateCityList()
        let expectedCount = 1
        let expectedCity = City(id: 3120304, name: "Irun", country: "Spain")
        
        // Act
        let result = sut.getCities(keyword: "Irun")
        
        // Assert
        XCTAssertEqual(result.count, expectedCount)
        XCTAssertEqual(result.first, expectedCity)
    }
    
    func test_getCities_withNameContainsGAL_shouldReturn2Cities() throws {
        // Arrange
        try populateCityList()
        let expectedCount = 2
        let expectedCities = [
            City(id: 3121766, name: "Galapagar", country: "Spain"),
            City(id: 3121751, name: "Galdakao", country: "Spain")
        ]
        
        // Act
        let result = sut.getCities(keyword: "GAL")
        
        // Assert
        XCTAssertEqual(result.count, expectedCount)
        XCTAssertEqual(result, expectedCities)
    }
    
    func test_getCities_withNameContainsNew_shouldReturnNoCity() throws {
        // Arrange
        try populateCityList()
        let expectedCount = 0
        
        // Act
        let result = sut.getCities(keyword: "New")
        
        // Assert
        XCTAssertEqual(result.count, expectedCount)
    }
    
    func test_getSearchHistory_withNoSavedCity_shouldReturnEmpty() throws {
        // Arrange
        let expectedCount = 0
        
        // Act
        let searchedCities = sut.getSearchedCity()
        
        // Assert
        XCTAssertEqual(searchedCities.count, expectedCount)
    }
    
    func test_getSearchHistory_with1SavedCity_shouldReturn1City() throws {
        // Arrange
        let today = Date()
        try populateSearchHistory(createdAt: today)
        let expectedCount = 1
        let exepectedSearchHistory = SearchCityHistory(
            city: City(id: 3121766, name: "Galapagar", country: "Spain"),
            createdAt: today
        )
        
        // Act
        let searchedCities = sut.getSearchedCity()
        
        // Assert
        XCTAssertEqual(searchedCities.count, expectedCount)
        XCTAssertEqual(searchedCities.first, exepectedSearchHistory.city)
    }
    
    func test_saveSearchHistory_with1NewCity() throws {
        // Arrange
        try populateCityList()
        let expectedCount = 1
        let exepectedSearchHistory = SearchCityHistory(
            city: City(id: 3121766, name: "Galapagar", country: "Spain"),
            createdAt: Date()
        )
        
        // Act
        sut.saveSearchHistory(cityId: 3121766)
        
        // Assert
        let searchHistory = realmStorage.fetch(SearchCityHistory.self)
        XCTAssertEqual(searchHistory.count, expectedCount)
        XCTAssertEqual(searchHistory.first, exepectedSearchHistory)
    }
    
    func test_saveSearchHistory_with1SavedCityBefore() throws {
        // Arrange
        let today = Date()
        let createdAt = today.addingTimeInterval(-300)
        try populateSearchHistory(createdAt: createdAt)
        let expectedCount = 1
        let exepectedSearchHistory = SearchCityHistory(
            city: City(id: 3121766, name: "Galapagar", country: "Spain"),
            createdAt: today
        )
            
        // Act
        sut.saveSearchHistory(cityId: 3121766)
        
        // Assert
        let searchHistory = realmStorage.fetch(SearchCityHistory.self)
        XCTAssertEqual(searchHistory.count, expectedCount)
        XCTAssertEqual(searchHistory.first, exepectedSearchHistory)
    }
}

private extension CityInteractorTests {
    
    func populateCityList() throws {
        let testBundle = Bundle(for: type(of: self))
        let listingJsonPath = testBundle.url(forResource: "city_list_mock", withExtension: "json")!
        let cities = try JSONDecoder().decode([City].self, from: Data(contentsOf: listingJsonPath))
        try realmStorage.save(objects: cities)
    }
    
    func populateSearchHistory(createdAt: Date) throws {
        let searchHistory = SearchCityHistory(
            city: City(id: 3121766, name: "Galapagar", country: "Spain"),
            createdAt: createdAt
        )
        try realmStorage.save(object: searchHistory)
    }
}
