//
//  CityViewModelTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 17/04/2021.
//

import XCTest
@testable import iOS_NAB

class CityViewModelTests: XCTestCase {

    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func test_viewModels_withSameContent_shouldBeEqual() throws {
        // Arrange
        let vm1 = CityViewModel(city: Defaults.cities[0], isSearchHistory: false)
        let vm2 = CityViewModel(city: Defaults.cities[0], isSearchHistory: false)
        
        // Act & Assert
        XCTAssertEqual(vm1, vm2)
        XCTAssertTrue(vm1.isContentEqual(to: vm2))
    }
    
    func test_viewModels_withDifferentContent_shouldBeDifferent() throws {
        // Arrange
        let vm1 = CityViewModel(city: Defaults.cities[0], isSearchHistory: false)
        let vm2 = CityViewModel(city: Defaults.cities[1], isSearchHistory: false)
        
        let vm3 = CityViewModel(city: Defaults.cities[1], isSearchHistory: false)
        let vm4 = CityViewModel(city: Defaults.cities[1], isSearchHistory: true)
        
        // Act & Assert
        XCTAssertNotEqual(vm1, vm2)
        XCTAssertFalse(vm1.isContentEqual(to: vm2))
        
        XCTAssertNotEqual(vm3, vm4)
        XCTAssertFalse(vm3.isContentEqual(to: vm4))
    }
}

private enum Defaults {
    static let cities = [
        City(id: 3121766, name: "Galapagar", country: "Spain"),
        City(id: 3121751, name: "Galdakao", country: "Spain")
    ]
}
