//
//  WeatherForecastModelTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 17/04/2021.
//

import XCTest
@testable import iOS_NAB

class WeatherForecastModelTests: XCTestCase {

    private lazy var dailyForecastResponse: [WeatherForecastResponse.Daily] = getDailyForecast()
    
    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func test_dailyForecastModel_withSameContent_shouldBeEqual() throws {
        // Arrange
        let model1 = DailyForecast(response: dailyForecastResponse[0])
        let model2 = DailyForecast(response: dailyForecastResponse[0])
        
        // Act & Assert
        XCTAssertEqual(model1, model2)
    }
    
    func test_dailyForecastModel_withDifferentContent_shouldBeDifferent() throws {
        // Arrange
        let model1 = DailyForecast(response: dailyForecastResponse[0])
        let model2 = DailyForecast(response: dailyForecastResponse[1])
        
        // Act & Assert
        XCTAssertNotEqual(model1, model2)
    }
    
    func test_dailyForecastModel_withResponseData() {
        // Arrange
        let model = DailyForecast(response: dailyForecastResponse[0])
        
        let date = Date(timeIntervalSince1970: 1485766800)
        let expectedModel = DailyForecast(date: date, averageTemp: "262", pressure: "1025", humidity: "76", evaluation: "sky is clear")
        
        // Act & Assert
        XCTAssertEqual(model, expectedModel)
    }
    
    func test_weatherForecaseModel_withSameContent_shouldBeEqual() throws {
        // Arrange
        let createdAt = Date()
        let forecastData = dailyForecastResponse.map { DailyForecast(response: $0) }
        let model1 = WeatherForecast(id: 12345,
                                     days: 7,
                                     tempUnit: "standard",
                                     createAt: createdAt,
                                     forecastData: forecastData)
        let model2 = WeatherForecast(id: 12345,
                                     days: 7,
                                     tempUnit: "standard",
                                     createAt: createdAt,
                                     forecastData: forecastData)
        
        // Act & Assert
        XCTAssertEqual(model1, model2)
    }
    
    func test_weatherForecaseModel_withDifferentContent_shouldBeEqualDifferent() throws {
        // Arrange
        let createdAt = Date()
        let createdAt2 = Date().addingTimeInterval(600)
        let forecastData = dailyForecastResponse.map { DailyForecast(response: $0) }
        let model1 = WeatherForecast(id: 12345,
                                     days: 7,
                                     tempUnit: "standard",
                                     createAt: createdAt,
                                     forecastData: forecastData)
        let model2 = WeatherForecast(id: 12345,
                                     days: 7,
                                     tempUnit: "imperial",
                                     createAt: createdAt2,
                                     forecastData: forecastData)
        
        // Act & Assert
        XCTAssertNotEqual(model1, model2)
    }
}

private extension WeatherForecastModelTests {
    
    func getDailyForecast() -> [WeatherForecastResponse.Daily] {
        let testBundle = Bundle(for: type(of: self))
        let listingJsonPath = testBundle.url(forResource: "daily_forecast_mock", withExtension: "json")!
        return try! JSONDecoder().decode([WeatherForecastResponse.Daily].self, from: Data(contentsOf: listingJsonPath))
    }
}
