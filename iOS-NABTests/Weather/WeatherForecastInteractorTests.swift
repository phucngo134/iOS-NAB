//
//  WeatherForecastInteractorTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 17/04/2021.
//

import XCTest
import RealmSwift
@testable import iOS_NAB

class WeatherForecastInteractorTests: XCTestCase {

    private var sut: WeatherForecastInteractor!
    
    private let settings = SettingsMock()
    private let connectivity = ConnectivityMock()
    
    private lazy var realmStorage: RealmStorageContext = {
        let configuration = Realm.Configuration(inMemoryIdentifier: "WeatherForecastInteractorTests")
        return RealmStorageContext(configuration: configuration)
    }()
    
    override func setUpWithError() throws {
        sut = WeatherForecastInteractor(connectivity: connectivity,
                                        settings: settings,
                                        storage: realmStorage)
    }

    override func tearDownWithError() throws {
        try realmStorage.reset()
        settings.reset()
        connectivity.reset()
    }

    func test_lastSelectedDays_withPreviousSavedData_shouldReturnCorrectData() throws {
        // Arrange
        let expectedDays: Int = 5
        settings.saveInt(key: .days, value: expectedDays)
        
        // Act
        let selectedDays = sut.lastSelectedDays
        
        // Assert
        XCTAssertEqual(selectedDays, expectedDays)
    }
    
    func test_lastSelectedDays_withNoSavedData_shouldReturnDefaultData() throws {
        // Arrange
        let expectedDays = WeatherForecastInteractor.Defaults.days
        
        // Act
        let selectedDays = sut.lastSelectedDays
        
        // Assert
        XCTAssertEqual(selectedDays, expectedDays)
    }
    
    func test_lastSelectedTempUnit_withPreviousSavedData_shouldReturnCorrectData() throws {
        // Arrange
        let expectedTempUnit: TempUnit = .imperial
        settings.saveString(key: .tempUnit, value: expectedTempUnit.rawValue)
        
        // Act
        let lastSelectedTempUnit = sut.lastSelectedTempUnit
        
        // Assert
        XCTAssertEqual(lastSelectedTempUnit, expectedTempUnit)
    }
    
    func test_lastSelectedTempUnit_withNoSavedData_shouldReturnDefaultData() throws {
        // Arrange
        let expectedTempUnit = WeatherForecastInteractor.Defaults.tempUnit
        
        // Act
        let lastSelectedTempUnit = sut.lastSelectedTempUnit
        
        // Assert
        XCTAssertEqual(lastSelectedTempUnit, expectedTempUnit)
    }
    
    func test_resetConfig() {
        // Arrange
        
        // Act
        sut.resetConfig()
        
        // Assert
        XCTAssertTrue(settings.defaults.isEmpty)
    }
    
    func test_shouldFetchWeatherForecast_withNoSavedData_shouldReturnTrue() {
        // Arrange
        
        // Act
        let result = sut.shouldFetchWeatherForecast(parameters: Defaults.params1)
        
        // Assert
        XCTAssertEqual(result, true)
    }
    
    func test_shouldFetchWeatherForecast_withSavedDataAndNotExpiredYet_shouldReturnFalse() throws {
        // Arrange
        let timeInterval = Double(WeatherForecastInteractor.Defaults.livingTime * 60) * 0.5
        let createdAt = Date().addingTimeInterval(-timeInterval)
        try populateWeatherForecast(createdAt: createdAt)
        
        // Act
        let result = sut.shouldFetchWeatherForecast(parameters: Defaults.params1)
        
        // Assert
        XCTAssertEqual(result, false)
    }
    
    func test_shouldFetchWeatherForecast_withSavedDataAndExpired_shouldReturnTrue() throws {
        // Arrange
        let timeInterval = Double(WeatherForecastInteractor.Defaults.livingTime * 60) * 1.5
        let createdAt = Date().addingTimeInterval(-timeInterval)
        try populateWeatherForecast(createdAt: createdAt)
        
        // Act
        let result = sut.shouldFetchWeatherForecast(parameters: Defaults.params1)
        
        // Assert
        XCTAssertEqual(result, true)
    }
    
    func test_getLocalWeatherForecast_withNoSavedData_shouldReturnEmpty() {
        // Arrange
        
        // Act
        let viewModels = sut.getLocalWeatherForecast(parameters: Defaults.params1)
        
        // Assert
        XCTAssertTrue(viewModels.isEmpty)
    }
    
    func test_getLocalWeatherForecast_withSavedData_shouldReturnSavedData() throws {
        // Arrange
        let expectedViewModels = getDailyForecastViewModels()
        let createdAt = Date()
        try populateWeatherForecast(createdAt: createdAt)
        
        // Act
        let viewModels = sut.getLocalWeatherForecast(parameters: Defaults.params1)
        
        // Assert
        XCTAssertEqual(viewModels, expectedViewModels)
    }
    
    func test_fetchWeatherForecast_withNoInternetConnection_shouldNotExecute() {
        // Arrange
        connectivity.isConnectedToInternet = false
        
        // Act & Assert
        sut.fetchWeatherForecast(parameters: Defaults.params1, isMocked: true, completion: { result in
            switch result {
            case .success:
                XCTFail("Should not fall into this case!")
                
            case let .failure(error):
                XCTAssertEqual(error as? NetworkError, .noConnection)
            }
        })
    }
    
    func test_fetchWeatherForecast_withMockedRequestAndNoSavedData() {
        // Arrange
        connectivity.isConnectedToInternet = true
        let expectedViewModels = getDailyForecastViewModels(isFull: true)
        let createdAt = Date()
        
        // Act & Assert
        sut.fetchWeatherForecast(parameters: Defaults.params1, isMocked: true) { [unowned self] result in
            switch result {
            case let .success(viewModels):
                XCTAssertEqual(viewModels, expectedViewModels)
                
                let localData = self.sut.getLocalWeatherForecast(parameters: Defaults.params1)
                XCTAssertEqual(localData, expectedViewModels)
                
                let realmData = self.realmStorage.fetch(WeatherForecast.self).first!
                XCTAssertEqual(
                    DateFormatter.fullDateFormatter.string(from: realmData.createdAt),
                    DateFormatter.fullDateFormatter.string(from: createdAt)
                )
                
            case .failure: break
            }
        }
    }
    
    func test_fetchWeatherForecast_withMockedRequestAndSavedData() throws {
        // Arrange
        connectivity.isConnectedToInternet = true
        let expectedViewModels = getDailyForecastViewModels(isFull: true)
        let createdAt = Date()
        try populateWeatherForecast(createdAt: createdAt.addingTimeInterval(-200))
        
        // Act & Assert
        sut.fetchWeatherForecast(parameters: Defaults.params1, isMocked: true) { [unowned self] result in
            switch result {
            case let .success(viewModels):
                XCTAssertEqual(viewModels, expectedViewModels)
                
                let localData = self.sut.getLocalWeatherForecast(parameters: Defaults.params1)
                XCTAssertEqual(localData, expectedViewModels)
                
                let realmData = self.realmStorage.fetch(WeatherForecast.self).first!
                XCTAssertEqual(
                    DateFormatter.fullDateFormatter.string(from: realmData.createdAt),
                    DateFormatter.fullDateFormatter.string(from: createdAt)
                )
                
            case .failure: break
            }
        }
    }
}

private extension WeatherForecastInteractorTests {
    
    enum Defaults {
        static let params1 = OpenWeatherAPI.Parameters(cityId: 12345, cityName: "Irun", days: 7, tempUnit: TempUnit.standard.rawValue)
    }
    
    func populateWeatherForecast(createdAt: Date, isFull: Bool = false) throws {
        let forecastData = getDailyForecast(isFull: isFull)
        let weatherForecast = getWeatherForecast(createdAt: createdAt, forecastData: forecastData)
        try realmStorage.save(object: weatherForecast)
    }
    
    func getWeatherForecast(createdAt: Date, forecastData: [DailyForecast]) -> WeatherForecast {
        let weatherForecast = WeatherForecast(id: Defaults.params1.cityId,
                                              days: Defaults.params1.days,
                                              tempUnit: Defaults.params1.tempUnit,
                                              createAt: createdAt,
                                              forecastData: forecastData)
        return weatherForecast
    }
    
    func getDailyForecastViewModels(isFull: Bool = false) -> [DailyForecastViewModel] {
        let forecastData = getDailyForecast(isFull: isFull)
        return forecastData.map { DailyForecastViewModel(model: $0,
                                                         tempUnit: Defaults.params1.tempUnit) }
    }
    
    func getDailyForecast(isFull: Bool = false) -> [DailyForecast] {
        let testBundle = Bundle(for: type(of: self))
        let fileName = isFull ? "weather_forecast_mock" : "daily_forecast_mock"
        let listingJsonPath = testBundle.url(forResource: fileName, withExtension: "json")!
        let jsonData = try! Data(contentsOf: listingJsonPath)
        
        if !isFull {
            let data = try! JSONDecoder().decode([WeatherForecastResponse.Daily].self, from: jsonData)
            return data.map { DailyForecast(response: $0) }
        } else {
            let data = try! JSONDecoder().decode(WeatherForecastResponse.self, from: jsonData)
            return data.list.map { DailyForecast(response: $0) }
        }
    }
}

private class ConnectivityMock: Reachability {
    var isConnectedToInternet: Bool = false
    
    func reset() {
        isConnectedToInternet = false
    }
}

private class SettingsMock: IntSettingsStoring & StringSettingsStoring {
    var defaults: [String: Any] = [:]
    
    func saveInt(key: SettingKeys, value: Int) {
        guard key == .days else { return }
        defaults[key.rawValue] = value
    }
    
    func loadInt(key: SettingKeys) -> Int {
        guard key == .days else { return 0 }
        return defaults[key.rawValue] as? Int ?? 0
    }
    
    func saveString(key: SettingKeys, value: String) {
        guard key == .tempUnit else { return }
        defaults[key.rawValue] = value
    }
    
    func loadString(key: SettingKeys) -> String? {
        guard key == .tempUnit else { return nil }
        return defaults[key.rawValue] as? String
    }
    
    func remove(key: SettingKeys) {
        defaults[key.rawValue] = nil
    }
    
    func reset() {
        defaults.removeAll()
    }
}
