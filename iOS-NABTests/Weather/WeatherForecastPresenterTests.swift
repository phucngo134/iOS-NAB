//
//  WeatherForecastPresenterTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 17/04/2021.
//

import XCTest
import DifferenceKit
@testable import iOS_NAB

class WeatherForecastPresenterTests: XCTestCase {

    private var sut: WeatherForecastPresenter!
    
    private let interactor = WeatherForecastInteractorMock()
    private let router = WeatherRouterMock()
    private let weatherForecastCell = WeatherForecastCellMock()
    private let stickyConfigView = StickyConfigViewMock()
    private let view = WeatherViewMock()
    
    override func setUpWithError() throws {
        sut = WeatherForecastPresenter(interactor: interactor, router: router)
    }

    override func tearDownWithError() throws {
        interactor.reset()
        router.reset()
        weatherForecastCell.reset()
        stickyConfigView.reset()
        view.reset()
    }

    func test_totalItems() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        let totalItems = sut.totalItems
        
        // Assert
        XCTAssertEqual(totalItems, Defaults.viewModels.count)
    }
    
    func test_displayWeatherForecast_withCorrectIndex() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        sut.displayWeatherForecast(weatherForecastCell, atIndex: 0)
        
        // Assert
        XCTAssertEqual(weatherForecastCell.setupWeatherForecastWasCalled, true)
    }
    
    func test_displayWeatherForecast_withIncorrectIndex() {
        // Arrange
        populateInternalViewModels()
        
        // Act
        sut.displayWeatherForecast(weatherForecastCell, atIndex: 2)
        
        // Assert
        XCTAssertEqual(weatherForecastCell.setupWeatherForecastWasCalled, false)
    }
    
    func test_displayWeatherForecast_withParameterNilAndNoPreviousViewModels() {
        // Arrange
        sut.view = view
        
        // Act
        sut.displayWeatherForecast(nil)
        
        // Assert
        XCTAssertEqual(view.reloadChangesWasCalled, false)
        XCTAssertEqual(view.hideAnyStateWasCalled, true)
    }
    
    func test_displayWeatherForecast_withParameterNilAndPreviousViewModels() {
        // Arrange
        populateInternalViewModels()
        sut.view = view
        
        // Act
        sut.displayWeatherForecast(nil)
        
        // Assert
        XCTAssertEqual(view.reloadChangesWasCalled, true)
        XCTAssertEqual(view.hideAnyStateWasCalled, false)
    }
    
    func test_displayWeatherForecast_withOneCityAndShouldNotFetchRequestAndNoInternalViewModels() {
        // Arrange
        sut.view = view
        interactor.shouldFetchWeatherForecast = false
        
        // Act
        sut.displayWeatherForecast(Defaults.cityViewModel)
        
        // Assert
        XCTAssertEqual(view.displayLoadingStateWasCalled, true)
        XCTAssertEqual(view.reloadChangesWasCalled, true)
    }
    
    func test_displayWeatherForecast_withOneCityAndShouldFetchRequestAndNoInternalViewModels() {
        // Arrange
        sut.view = view
        interactor.shouldFetchWeatherForecast = true
        
        // Act
        sut.displayWeatherForecast(Defaults.cityViewModel)
        
        // Assert
        XCTAssertEqual(view.displayLoadingStateWasCalled, true)
        XCTAssertEqual(interactor.fetchWeatherForecastWasCalled, true)
    }
    
    func test_displayStickyConfigView() {
        // Arrange
        interactor.lastSelectedDays = 12
        interactor.lastSelectedTempUnit = .imperial
        
        // Act
        sut.displayStickyConfigView(stickyConfigView)
        
        // Assert
        XCTAssertEqual(stickyConfigView.selectedDays, 12)
        XCTAssertEqual(stickyConfigView.selectedTempUnit, .imperial)
    }
    
    func test_resetConfig() {
        // Arrange
        
        // Act
        sut.resetConfig()
        
        // Assert
        XCTAssertEqual(interactor.resetConfigWasCalled, true)
    }
    
    func test_openNumberOfDaysForecastOptions() {
        // Arrange
    
        // Act
        sut.openNumberOfDaysForecastOptions()
        
        // Assert
        XCTAssertEqual(router.openNumberOfDaysForecastOptionsWasCalled, true)
    }
    
    func test_openTemperatureUnitOptions() {
        // Arrange
    
        // Act
        sut.openTemperatureUnitOptions()
        
        // Assert
        XCTAssertEqual(router.openTemperatureUnitOptionsWasCalled, true)
    }
}

private extension WeatherForecastPresenterTests {
    
    func populateInternalViewModels() {
        sut.syncData(Defaults.viewModels)
    }
}

private class WeatherViewMock: UIViewController, WeatherViewProtocol {
    var displayLoadingStateWasCalled: Bool = false
    var displayErrorStateWasCalled: Bool = false
    var hideAnyStateWasCalled: Bool = false
    var reloadChangesWasCalled: Bool = false
    
    func displayLoadingState() {
        displayLoadingStateWasCalled = true
    }
    
    func displayErrorState(errorMessage: String) {
        displayErrorStateWasCalled = true
    }
    
    func hideAnyState() {
        hideAnyStateWasCalled = true
    }
    
    func reload(changes: StagedChangeset<[DailyForecastViewModel]>) {
        reloadChangesWasCalled = true
    }
    
    func reset() {
        displayLoadingStateWasCalled = false
        displayErrorStateWasCalled = false
        hideAnyStateWasCalled = false
        reloadChangesWasCalled = false
    }
}

private class WeatherForecastInteractorMock: WeatherForecastInteraction {
    var lastSelectedDays: Int = Defaults.days
    var lastSelectedTempUnit: TempUnit = Defaults.tempUnit
    var shouldFetchWeatherForecast: Bool = false
    var resetConfigWasCalled: Bool = false
    var fetchWeatherForecastWasCalled: Bool = false
    
    func resetConfig() {
        resetConfigWasCalled = true
    }
    
    func shouldFetchWeatherForecast(parameters: OpenWeatherAPI.Parameters) -> Bool {
        return shouldFetchWeatherForecast
    }
    
    func getLocalWeatherForecast(parameters: OpenWeatherAPI.Parameters) -> [DailyForecastViewModel] {
        return Defaults.viewModels
    }
    
    func fetchWeatherForecast(parameters: OpenWeatherAPI.Parameters, isMocked: Bool, completion: @escaping (Result<[DailyForecastViewModel], Error>) -> Void) {
        fetchWeatherForecastWasCalled = true
    }
    
    func reset() {
        resetConfigWasCalled = false
        shouldFetchWeatherForecast = false
        fetchWeatherForecastWasCalled = false
    }
}

private class WeatherRouterMock: WeatherRoutes {
    
    var openNumberOfDaysForecastOptionsWasCalled: Bool = false
    var openTemperatureUnitOptionsWasCalled: Bool = false
    
    func openNumberOfDaysForecastOptions(onChangeData: OnChangeDataAction?) {
        openNumberOfDaysForecastOptionsWasCalled = true
    }
    
    func openTemperatureUnitOptions(onChangeData: OnChangeDataAction?) {
        openTemperatureUnitOptionsWasCalled = true
    }
    
    func reset() {
        openNumberOfDaysForecastOptionsWasCalled = false
        openTemperatureUnitOptionsWasCalled = false
    }
}

private class WeatherForecastCellMock: WeatherForecastCellDisplay {
    static var reuseIdentifier: String = "WeatherForecastCellMock"
    
    var setupWeatherForecastWasCalled: Bool = false
    
    func setupWeatherForecast(_ viewModel: DailyForecastViewModel) {
        setupWeatherForecastWasCalled = true
    }
    
    func reset() {
        setupWeatherForecastWasCalled = false
    }
}

private class StickyConfigViewMock: StickyConfigDisplay {
    var onDaysTap: (() -> Void)?
    var onUnitTap: (() -> Void)?
    
    var selectedDays: Int = 0
    var selectedTempUnit: TempUnit = .standard
    
    func display(selectedDays days: Int, selectedTempUnit tempUnit: TempUnit) {
        selectedDays = days
        selectedTempUnit = tempUnit
    }
    
    func reset() {
        selectedDays = 0
        selectedTempUnit = .standard
    }
}

private enum Defaults {
    static let days: Int = 7
    static let tempUnit: TempUnit = .standard
    static let cityViewModel = CityViewModel(
        city: City(id: 3121766, name: "Galapagar", country: "Spain"),
        isSearchHistory: false
    )
    static let viewModels: [DailyForecastViewModel] = [
        DailyForecastViewModel(date: "Sat, 17 Apr 2021",
                               averageTemp: "301K",
                               pressure: "1020",
                               humidity: "61%",
                               evaluation: "light rain"),
        DailyForecastViewModel(date: "Sun, 18 Apr 2021",
                               averageTemp: "300K",
                               pressure: "1016",
                               humidity: "71%",
                               evaluation: "light rain"),
    ]
}
