//
//  DailyForecastViewModelTests.swift
//  iOS-NABTests
//
//  Created by Phuc Ngo on 17/04/2021.
//

import XCTest
@testable import iOS_NAB

class DailyForecastViewModelTests: XCTestCase {

    private lazy var dailyForecastData: [DailyForecast] = getDailyForecast()
    
    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func test_dailyForecastViewModel_withSameContent_shouldBeEqual() throws {
        // Arrange
        let model1 = DailyForecastViewModel(model: dailyForecastData[0], tempUnit: TempUnit.standard.rawValue)
        let model2 = DailyForecastViewModel(model: dailyForecastData[0], tempUnit: TempUnit.standard.rawValue)
       
        // Act & Assert
        XCTAssertEqual(model1, model2)
        XCTAssertTrue(model1.isContentEqual(to: model2))
    }
    
    func test_dailyForecastViewModel_withDifferentContent_shouldBeDifferent() throws {
        // Arrange
        let model1 = DailyForecastViewModel(model: dailyForecastData[0], tempUnit: TempUnit.standard.rawValue)
        let model2 = DailyForecastViewModel(model: dailyForecastData[1], tempUnit: TempUnit.standard.rawValue)
       
        // Act & Assert
        XCTAssertNotEqual(model1, model2)
        XCTAssertFalse(model1.isContentEqual(to: model2))
    }
    
    func test_dailyForecastViewModel_withReponseData() {
        // Arrange
        let model = DailyForecastViewModel(model: dailyForecastData[0], tempUnit: TempUnit.standard.rawValue)
        let expectedModel = DailyForecastViewModel(date: "Mon, 30 Jan 2017", averageTemp: "262K", pressure: "1025", humidity: "76%", evaluation: "sky is clear")
        
        // Act & Assert
        XCTAssertEqual(model, expectedModel)
        XCTAssertTrue(model.isContentEqual(to: expectedModel))
    }
}

private extension DailyForecastViewModelTests {
    
    func getDailyForecast() -> [DailyForecast] {
        let testBundle = Bundle(for: type(of: self))
        let listingJsonPath = testBundle.url(forResource: "daily_forecast_mock", withExtension: "json")!
        let data = try! JSONDecoder().decode([WeatherForecastResponse.Daily].self, from: Data(contentsOf: listingJsonPath))
        return data.map { DailyForecast(response: $0) }
    }
}
