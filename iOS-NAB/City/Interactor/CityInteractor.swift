//
//  CityInteractor.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

protocol CityInteraction {
    func getCities(keyword: String) -> [City]
    func getSearchedCity() -> [City]
    func saveSearchHistory(cityId: Int)
}

class CityInteractor {
    private let storage: StorageContext
    private var cities: [City] = []
    
    init(storage: StorageContext) {
        self.storage = storage
    }
}

extension CityInteractor: CityInteraction {
    
    func getCities(keyword: String) -> [City] {
        let sorted = Sorted(key: CityColumn.name.rawValue, ascending: true)
        let predicate = NSPredicate(format: "\(CityColumn.name.rawValue) CONTAINS[c] %@", keyword)
        let results = storage.fetch(City.self, predicate: predicate, sorted: sorted)
        return results
    }
    
    func getSearchedCity() -> [City] {
        let sorted = Sorted(key: SearchCityHistoryColumn.createdAt.rawValue, ascending: false)
        let results = storage.fetch(SearchCityHistory.self, sorted: sorted)
        return results.compactMap { $0.city }
    }
    
    func saveSearchHistory(cityId: Int) {
        let predicate = NSPredicate(format: "city.id = %d", cityId)
        if let searchHistory = storage.fetch(SearchCityHistory.self, predicate: predicate).first {
            try? storage.update {
                searchHistory.createdAt = Date()
            }
        } else {
            guard let city = storage.fetchById(City.self, id: cityId) else { return }
            let searchHistory = SearchCityHistory(city: city, createdAt: Date())
            try? storage.save(object: searchHistory)
        }
    }
}
