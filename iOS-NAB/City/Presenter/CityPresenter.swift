//
//  CityPresenter.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import DifferenceKit

protocol CityPresentation {
    var totalItems: Int { get }
    func syncData(cities: [CityViewModel])
    func displayCities(keyword: String)
    func displayCity(_ cellDisplay: CityCellDisplay, atIndex index: Int)
    func city(atIndex index: Int) -> CityViewModel?
    func displaySearchHistory()
    func saveSearchHistory(atIndex index: Int)
}

class CityPresenter: CityPresentation {
    
    private let interactor: CityInteraction
    private var cities: [CityViewModel] = []
    weak var view: CityViewProtocol?
    
    init(interactor: CityInteraction) {
        self.interactor = interactor
    }
    
    var totalItems: Int {
        return cities.count
    }
    
    func syncData(cities: [CityViewModel]) {
        self.cities = cities
    }
    
    func displayCities(keyword: String) {
        guard !keyword.isEmpty else {
            displaySearchHistory()
            return
        }
        let fetchedCities = interactor.getCities(keyword: keyword)
            .map { CityViewModel(city: $0, isSearchHistory: false) }
        let changes = StagedChangeset<[CityViewModel]>(source: cities, target: fetchedCities)
        if changes.isEmpty {
            view?.hideAnyState()
        } else {
            view?.reload(changes: changes)
        }
        if fetchedCities.isEmpty {
            view?.displayNoCityFoundState()
        }
    }
    
    func displayCity(_ cellDisplay: CityCellDisplay, atIndex index: Int) {
        guard cities.indices.contains(index) else { return }
        let city = cities[index]
        cellDisplay.setupCity(city)
    }
    
    func city(atIndex index: Int) -> CityViewModel? {
        guard cities.indices.contains(index) else { return nil }
        let city = cities[index]
        return city
    }
    
    func displaySearchHistory() {
        let searchedCities = interactor.getSearchedCity()
            .map { CityViewModel(city: $0, isSearchHistory: true) }
        let changes = StagedChangeset<[CityViewModel]>(source: cities, target: searchedCities)
        view?.reload(changes: changes)
    }
    
    func saveSearchHistory(atIndex index: Int) {
        guard cities.indices.contains(index) else { return }
        let city = cities[index]
        interactor.saveSearchHistory(cityId: city.id)
    }
}
