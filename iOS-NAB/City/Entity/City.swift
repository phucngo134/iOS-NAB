//
//  City.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import RealmSwift

enum CityColumn: String {
    case id
    case name
    case country
}

class City: Object, Decodable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var country: String = ""
    
    override class func primaryKey() -> String? {
        return CityColumn.id.rawValue
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "geonameid"
        case name
        case country
    }
    
    convenience init(id: Int, name: String, country: String) {
        self.init()
        self.id = id
        self.name = name
        self.country = country
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? City else { return false }
        return id == rhs.id &&
            name == rhs.name &&
            country == rhs.country
    }
}
