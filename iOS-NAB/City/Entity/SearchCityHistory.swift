//
//  SearchCityHistory.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import RealmSwift

enum SearchCityHistoryColumn: String {
    case createdAt
}

class SearchCityHistory: Object {
    @objc dynamic var city: City?
    @objc dynamic var createdAt = Date(timeIntervalSince1970: 1)
    
    convenience init(city: City, createdAt: Date) {
        self.init()
        self.city = city
        self.createdAt = createdAt
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? SearchCityHistory else { return false }
        let dateFormatter = DateFormatter.fullDateFormatter
        return city == rhs.city
            && dateFormatter.string(from: createdAt) == dateFormatter.string(from: rhs.createdAt)
    }
}
