//
//  CityViewModel.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import DifferenceKit

struct CityViewModel {
    let id: Int
    let name: String
    let country: String
    let isSearchHistory: Bool
    
    init(city: City, isSearchHistory: Bool) {
        id = city.id
        name = city.name
        country = city.country
        self.isSearchHistory = isSearchHistory
    }
}

extension CityViewModel: Equatable {
    static func == (lhs: CityViewModel, rhs: CityViewModel) -> Bool {
        return lhs.isContentEqual(to: rhs)
    }
}

extension CityViewModel: Differentiable {
    
    var differenceIdentifier: Int {
        return id
    }
    
    func isContentEqual(to source: CityViewModel) -> Bool {
        return id == source.id
            && name == source.name
            && country == source.country
            && isSearchHistory == source.isSearchHistory
    }
}
