//
//  CityCell.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit

protocol CityCellDisplay {
    static var reuseIdentifier: String { get }
    func setupCity(_ viewModel: CityViewModel)
}

class CityCell: UITableViewCell {
    
    private enum Defaults {
        static let titleFont = UIFont.preferredFont(forTextStyle: .headline)
        static let subtitleFont = UIFont.preferredFont(forTextStyle: .subheadline)
        static let iconSize: CGFloat = 24.0
    }
    
    private lazy var cityLabel: UILabel = createLabel()
    private lazy var countryLabel: UILabel = createLabel()
    private lazy var iconView: UIImageView = createImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureLayout()
        configureDisplay()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension CityCell {
    
    func configureLayout() {
        contentView.addSubview(cityLabel)
        contentView.addSubview(countryLabel)
        contentView.addSubview(iconView)
        
        cityLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(CGFloat.small)
            make.leading.equalTo(iconView.snp.trailing).offset(CGFloat.regular)
            make.trailing.equalToSuperview().offset(-CGFloat.regular)
        }
        countryLabel.snp.makeConstraints { make in
            make.top.equalTo(cityLabel.snp.bottom).offset(CGFloat.xSmall)
            make.bottom.equalToSuperview().offset(-CGFloat.small)
            make.leading.trailing.equalTo(cityLabel)
        }
        iconView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(CGFloat.regular)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(Defaults.iconSize)
        }
    }
    
    func configureDisplay() {
        cityLabel.font = Defaults.titleFont
        countryLabel.font = Defaults.subtitleFont
        iconView.tintColor = .gray
    }
    
    func createLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontForContentSizeCategory = true
        return label
    }
    
    func createImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.adjustsImageSizeForAccessibilityContentSizeCategory = true
        return imageView
    }
}

extension CityCell: CityCellDisplay {
    
    static var reuseIdentifier: String {
        return "CityCell"
    }
    
    func setupCity(_ viewModel: CityViewModel) {
        cityLabel.attributedText = NSAttributedString(text: viewModel.name, font: cityLabel.font)
        cityLabel.accessibilityValue = viewModel.name
        
        countryLabel.attributedText = NSAttributedString(text: viewModel.country, font: countryLabel.font)
        countryLabel.accessibilityValue = viewModel.country
        
        iconView.image = viewModel.isSearchHistory ? UIImage(named: "History") : UIImage(named: "Search")
        
        accessibilityLabel = accessibilityContentFor(viewModel)
    }
}

private extension CityCell {
    
    func accessibilityContentFor(_ viewModel: CityViewModel) -> String {
        var content: [String] = []
        
        let iconText = viewModel.isSearchHistory ? Accessibility.Label.history : Accessibility.Label.result
        content.append(iconText)
        
        let cityText = Accessibility.Label.city + viewModel.name
        content.append(cityText)
        
        let countryText = Accessibility.Label.country + viewModel.country
        content.append(countryText)
        
        return content.joined(separator: "\n")
    }
}
