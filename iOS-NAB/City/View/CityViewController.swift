//
//  CityViewController.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit
import SnapKit
import DifferenceKit

protocol CityViewProtocol: UIViewController, UISearchBarDelegate {
    var onSelectCity: ((CityViewModel) -> Void)? { get set }
    func displayNoCityFoundState()
    func hideAnyState()
    func reload(changes: StagedChangeset<[CityViewModel]>)
}

class CityViewController: UIViewController, StateDisplay {

    private let presenter: CityPresentation
    var onSelectCity: ((CityViewModel) -> Void)?
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    init(presenter: CityPresentation) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDisplay()
        configureLayout()
        configureKeyboardNotification()
    }
}

private extension CityViewController {
    
    func configureDisplay() {
        view.backgroundColor = .white
        navigationItem.title = NavigationTitle.city
        tableView.register(CityCell.self, forCellReuseIdentifier: CityCell.reuseIdentifier)
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
    }
    
    func configureLayout() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func configureKeyboardNotification() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(updateContentInsetForKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(updateContentInsetForKeyboard(notification:)), name: UIResponder.keyboardDidChangeFrameNotification, object: nil)
    }
        
    @objc func updateContentInsetForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            tableView.contentInset = .zero
        } else {
            let bottom: CGFloat = keyboardViewEndFrame.height - view.safeAreaInsets.bottom
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bottom, right: 0)
        }
        tableView.scrollIndicatorInsets = tableView.contentInset
    }
    
    @objc func displayCities(keywords: String) {
        presenter.displayCities(keyword: keywords)
    }
}

// MARK: - UITableViewDataSource
extension CityViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.totalItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CityCell.reuseIdentifier, for: indexPath) as? CityCell {
            presenter.displayCity(cell, atIndex: indexPath.row)
            return cell
        }
        fatalError("Not found cell!")
    }
}

// MARK: - UITableViewDelegate
extension CityViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let index = indexPath.row
        guard let selectedCity = presenter.city(atIndex: index) else { return }
        presenter.saveSearchHistory(atIndex: index)
        onSelectCity?(selectedCity)
    }
}

// MARK: - UISearchBarDelegate
extension CityViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        presenter.displaySearchHistory()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            presenter.displaySearchHistory()
            return
        }
        guard searchText.count > 2 else {
            hideState()
            return
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.perform(#selector(displayCities(keywords:)), with: searchText, afterDelay: 0.3)
    }
}

extension CityViewController: CityViewProtocol {
        
    func displayNoCityFoundState() {
        let state = State.error(title: StateText.noCityTitle,
                                message: StateText.noCityMessage)
        displayState(state)
    }
    
    func hideAnyState() {
        hideState()
    }
    
    func reload(changes: StagedChangeset<[CityViewModel]>) {
        tableView.reload(using: changes, with: .none) { [weak self] data in
            self?.presenter.syncData(cities: data)
            self?.hideState()
        }
    }
}
