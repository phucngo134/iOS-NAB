//
//  CityModule.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

class CityModule {
    private(set) var viewController: CityViewProtocol
    
    init() {
        let storage = StorageProvider().makeStorage()
        let interactor = CityInteractor(storage: storage)
        let presenter = CityPresenter(interactor: interactor)
        let viewController = CityViewController(presenter: presenter)
        presenter.view = viewController
        self.viewController = viewController
    }
}
