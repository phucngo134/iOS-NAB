//
//  UnitOptionsInteractor.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

protocol UnitOptionsInteraction {
    var lastSelectedTempUnit: TempUnit { get set }
}

class UnitOptionsInteractor {
    
    enum Defaults {
        static let tempUnit: TempUnit = .standard
    }
    
    private let settings: StringSettingsStoring
    
    init(settings: StringSettingsStoring) {
        self.settings = settings
    }
}

extension UnitOptionsInteractor: UnitOptionsInteraction {
    
    var lastSelectedTempUnit: TempUnit {
        get {
            guard let selectedTempUnit = settings.loadString(key: .tempUnit) else {
                return Defaults.tempUnit
            }
            return TempUnit.allCases.first(where: { $0.rawValue == selectedTempUnit }) ?? Defaults.tempUnit
        }
        set {
            settings.saveString(key: .tempUnit, value: newValue.rawValue)
        }
    }
}
