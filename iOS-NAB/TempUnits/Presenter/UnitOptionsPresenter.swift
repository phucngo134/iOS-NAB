//
//  UnitOptionsPresenter.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import DifferenceKit

protocol UnitOptionsPresentation {
    var totalItems: Int { get }
    func prepareData()
    func syncData(_ data: [OptionViewModel])
    func displayOption(_ cellDisplay: OptionCellDisplay, atIndex index: Int)
    func saveSelectedUnit(atIndex index: Int)
    func popViewController()
}

class UnitOptionsPresenter {
    private let router: RouterProtocol
    private var interactor: UnitOptionsInteraction
    private var viewModels: [OptionViewModel] = []
    weak var view: UnitOptionsViewProtocol?
    private lazy var availabelUnits: [TempUnit] = TempUnit.allCases
    
    init(interactor: UnitOptionsInteraction, router: RouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

extension UnitOptionsPresenter: UnitOptionsPresentation {
    var totalItems: Int {
        return viewModels.count
    }
    
    func prepareData() {
        let lastSelectedUnit = interactor.lastSelectedTempUnit
        let target = availabelUnits.map {
            OptionViewModel(name: "\($0.longTextDisplay)", isSelected: $0 == lastSelectedUnit)
        }
        let changes = StagedChangeset<[OptionViewModel]>(source: viewModels, target: target)
        view?.reload(changes: changes)
    }
    
    func syncData(_ data: [OptionViewModel]) {
        viewModels = data
    }
    
    func displayOption(_ cellDisplay: OptionCellDisplay, atIndex index: Int) {
        guard viewModels.indices.contains(index) else { return }
        let viewModel = viewModels[index]
        cellDisplay.setupOption(viewModel)
    }
    
    func saveSelectedUnit(atIndex index: Int) {
        guard availabelUnits.indices.contains(index) else { return }
        let tempUnit = availabelUnits[index]
        let lastSelectedTempUnit = interactor.lastSelectedTempUnit
        interactor.lastSelectedTempUnit = tempUnit
        view?.updateUI()
        if tempUnit != lastSelectedTempUnit {
            view?.onChangeData?()
        }
    }
    
    func popViewController() {
        router.popViewController()
    }
}
