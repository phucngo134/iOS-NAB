//
//  TempUnit.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

enum TempUnit: String, CaseIterable {
    case standard
    case metric
    case imperial
    
    var longTextDisplay: String {
        switch self {
        case .standard: return "Kelvin"
        case .metric: return "Celsius"
        case .imperial: return "Fahrenheit"
        }
    }
    
    var shortTextDisplay: String {
        switch self {
        case .standard: return UnitTemperature.kelvin.symbol
        case .metric: return UnitTemperature.celsius.symbol
        case .imperial: return UnitTemperature.fahrenheit.symbol
        }
    }
}
