//
//  UnitOptionsModule.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

class UnitOptionsModule {
    private(set) var viewController: UnitOptionsViewProtocol
    
    init() {
        let settings = UserDefaultSettings()
        let interactor = UnitOptionsInteractor(settings: settings)
        let router = UnitOptionsRouter()
        let presenter = UnitOptionsPresenter(interactor: interactor, router: router)
        let viewController = UnitOptionsViewController(presenter: presenter)
        presenter.view = viewController
        router.viewController = viewController
        self.viewController = viewController
    }
}
