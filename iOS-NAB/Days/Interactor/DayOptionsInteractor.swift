//
//  DayOptionsInteractor.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

protocol DayOptionsInteraction {
    var lastSelectedDays: Int { get set }
}

class DayOptionsInteractor {
    
    enum Defaults {
        static let days: Int = 7
    }
    
    private let settings: IntSettingsStoring
    
    init(settings: IntSettingsStoring) {
        self.settings = settings
    }
}

extension DayOptionsInteractor: DayOptionsInteraction {
    
    var lastSelectedDays: Int {
        get {
            let selectedDays = settings.loadInt(key: .days)
            guard selectedDays > 0 else {
                return Defaults.days
            }
            return selectedDays
        }
        set {
            settings.saveInt(key: .days, value: newValue)
        }
    }
}
