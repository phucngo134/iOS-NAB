//
//  DayOptionsModule.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

class DayOptionsModule {
    private(set) var viewController: DayOptionsViewProtocol
    
    init() {
        let settings = UserDefaultSettings()
        let interactor = DayOptionsInteractor(settings: settings)
        let router = DayOptionsRouter()
        let presenter = DayOptionsPresenter(interactor: interactor, router: router)
        let viewController = DayOptionsViewController(presenter: presenter)
        presenter.view = viewController
        router.viewController = viewController
        self.viewController = viewController
    }
}
