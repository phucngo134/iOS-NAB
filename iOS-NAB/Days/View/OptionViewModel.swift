//
//  OptionViewModel.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import DifferenceKit

struct OptionViewModel {
    let name: String
    let isSelected: Bool
}

extension OptionViewModel: Equatable {
    static func == (lhs: OptionViewModel, rhs: OptionViewModel) -> Bool {
        return lhs.isContentEqual(to: rhs)
    }
}

extension OptionViewModel: Differentiable {
    
    var differenceIdentifier: String {
        return name
    }
    
    func isContentEqual(to source: OptionViewModel) -> Bool {
        return name == source.name
            && isSelected == source.isSelected
    }
}
