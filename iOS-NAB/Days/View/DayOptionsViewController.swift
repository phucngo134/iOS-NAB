//
//  DayOptionsViewController.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit
import DifferenceKit
import SnapKit

protocol DayOptionsViewProtocol: UIViewController {
    var onChangeData: OnChangeDataAction? { get set }
    func reload(changes: StagedChangeset<[OptionViewModel]>)
    func updateUI()
}

class DayOptionsViewController: UIViewController {

    private let presenter: DayOptionsPresentation
    var onChangeData: OnChangeDataAction?
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    init(presenter: DayOptionsPresentation) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDisplay()
        configureLayout()
        presenter.prepareData()
    }
}

private extension DayOptionsViewController {
    
    func configureDisplay() {
        view.backgroundColor = .white
        navigationItem.title = NavigationTitle.days
        tableView.register(OptionCell.self, forCellReuseIdentifier: OptionCell.reuseIdentifier)
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
    }
    
    func configureLayout() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

// MARK: - UITableViewDataSource
extension DayOptionsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.totalItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: OptionCell.reuseIdentifier, for: indexPath) as? OptionCell {
            presenter.displayOption(cell, atIndex: indexPath.row)
            return cell
        }
        fatalError("Not found cell!")
    }
}

// MARK: - UITableViewDelegate
extension DayOptionsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        presenter.saveSelectedDays(atIndex: index)
        presenter.popViewController()
    }
}

extension DayOptionsViewController: DayOptionsViewProtocol {
     
    func reload(changes: StagedChangeset<[OptionViewModel]>) {
        tableView.reload(using: changes, with: .none) { [weak self] data in
            self?.presenter.syncData(data)
        }
    }
    
    func updateUI() {
        presenter.prepareData()
    }
}
