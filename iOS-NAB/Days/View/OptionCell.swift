//
//  OptionCell.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit

protocol OptionCellDisplay {
    static var reuseIdentifier: String { get }
    func setupOption(_ viewModel: OptionViewModel)
}

class OptionCell: UITableViewCell {

    private enum Defaults {
        static let titleFont = UIFont.preferredFont(forTextStyle: .callout)
        static let iconSize: CGFloat = 24.0
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureLayout()
        configureDisplay()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.adjustsFontForContentSizeCategory = true
        label.isAccessibilityElement = true
        return label
    }()
    
    private lazy var checkmarkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.adjustsImageSizeForAccessibilityContentSizeCategory = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
}

private extension OptionCell {
    
    func configureLayout() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(checkmarkImageView)
        
        nameLabel.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().offset(CGFloat.regular)
            make.bottom.equalToSuperview().offset(-CGFloat.regular)
        }
        checkmarkImageView.snp.makeConstraints { make in
            make.leading.equalTo(nameLabel.snp.trailing).offset(CGFloat.regular)
            make.trailing.equalToSuperview().offset(-CGFloat.regular)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(Defaults.iconSize)
        }
    }
    
    func configureDisplay() {
        selectionStyle = .none
        nameLabel.font = Defaults.titleFont
        checkmarkImageView.image = UIImage(named: "Checkmark")
        checkmarkImageView.tintColor = .blue
    }
}

extension OptionCell: OptionCellDisplay {
    
    static var reuseIdentifier: String {
        return "OptionCell"
    }
    
    func setupOption(_ viewModel: OptionViewModel) {
        nameLabel.attributedText = NSAttributedString(text: viewModel.name, font: nameLabel.font)
        nameLabel.accessibilityValue = viewModel.name
        
        checkmarkImageView.isHidden = !viewModel.isSelected
    }
}

private extension OptionCell {
    
    func accessibilityContentFor(_ viewModel: OptionViewModel) -> String {
        let selectedStateText = viewModel.isSelected ? Accessibility.Label.selected : Accessibility.Label.unselected
        let nameOptionText = viewModel.name
        return selectedStateText + " " + nameOptionText
    }
}
