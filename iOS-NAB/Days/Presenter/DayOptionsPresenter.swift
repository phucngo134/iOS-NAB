//
//  DayOptionsPresenter.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import DifferenceKit

protocol DayOptionsPresentation {
    var totalItems: Int { get }
    func prepareData()
    func syncData(_ data: [OptionViewModel])
    func displayOption(_ cellDisplay: OptionCellDisplay, atIndex index: Int)
    func saveSelectedDays(atIndex index: Int)
    func popViewController()
}

class DayOptionsPresenter {
    
    private enum Defaults {
        static let minDays: Int = 1
        static let maxDays: Int = 17
    }
    
    private let router: RouterProtocol
    private var interactor: DayOptionsInteraction
    private var viewModels: [OptionViewModel] = []
    weak var view: DayOptionsViewProtocol?
    private lazy var availabelDays: [Int] = Array(Defaults.minDays...Defaults.maxDays)
    
    init(interactor: DayOptionsInteraction, router: RouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

extension DayOptionsPresenter: DayOptionsPresentation {
    
    var totalItems: Int {
        return viewModels.count
    }
    
    func prepareData() {
        let lastSelectedDays = interactor.lastSelectedDays
        let target = availabelDays.map {
            OptionViewModel(
                name: "\($0) \($0 > 1 ? "days" : "day")",
                isSelected: $0 == lastSelectedDays
            )
        }
        let changes = StagedChangeset<[OptionViewModel]>(source: viewModels, target: target)
        view?.reload(changes: changes)
    }
    
    func syncData(_ data: [OptionViewModel]) {
        viewModels = data
    }
    
    func displayOption(_ cellDisplay: OptionCellDisplay, atIndex index: Int) {
        guard viewModels.indices.contains(index) else { return }
        let viewModel = viewModels[index]
        cellDisplay.setupOption(viewModel)
    }
    
    func saveSelectedDays(atIndex index: Int) {
        guard availabelDays.indices.contains(index) else { return }
        let days = availabelDays[index]
        let lastSelectedDays = interactor.lastSelectedDays
        interactor.lastSelectedDays = days
        view?.updateUI()
        if days != lastSelectedDays {
            view?.onChangeData?()
        }
    }
    
    func popViewController() {
        router.popViewController()
    }
}
