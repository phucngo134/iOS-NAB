//
//  UserDefaultSettings.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

enum SettingKeys: String {
    case days
    case tempUnit
}

protocol IntSettingsStoring {
    func saveInt(key: SettingKeys, value: Int)
    func loadInt(key: SettingKeys) -> Int
    func remove(key: SettingKeys)
}

protocol StringSettingsStoring {
    func saveString(key: SettingKeys, value: String)
    func loadString(key: SettingKeys) -> String?
    func remove(key: SettingKeys)
}

class UserDefaultSettings {
    
    private lazy var defaults: UserDefaults = {
        return UserDefaults.standard
    }()
}

extension UserDefaultSettings: IntSettingsStoring, StringSettingsStoring {
    
    func saveInt(key: SettingKeys, value: Int) {
        defaults.set(value, forKey: key.rawValue)
    }
    
    func loadInt(key: SettingKeys) -> Int {
        return defaults.integer(forKey: key.rawValue)
    }

    func saveString(key: SettingKeys, value: String) {
        defaults.set(value, forKey: key.rawValue)
    }
    
    func loadString(key: SettingKeys) -> String? {
        return defaults.string(forKey: key.rawValue)
    }
    
    func remove(key: SettingKeys) {
        defaults.removeObject(forKey: key.rawValue)
    }
}
