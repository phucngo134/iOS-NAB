//
//  RouterProtocol.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit

protocol RouterProtocol: class {
    var viewController: UIViewController? { get }
    func popViewController()
}

extension RouterProtocol {
    
    func popViewController() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
