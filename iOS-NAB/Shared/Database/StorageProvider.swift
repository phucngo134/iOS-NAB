//
//  StorageProvider.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

class StorageProvider {
    func makeStorage() -> StorageContext {
        return RealmStorageContext()
    }
}
