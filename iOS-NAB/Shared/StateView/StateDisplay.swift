//
//  StateDisplay.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit

protocol StateDisplay {
    func displayState(_ state: State)
    func hideState()
}

extension StateDisplay where Self: UIViewController {
    
    func displayState(_ state: State) {
        // Remove exisitng state if available
        hideState()
        // Display new state
        let stateViewController = StateViewController(state: state)
        add(stateViewController)
    }
    
    func hideState() {
        if let child = children.first(where: { $0.isKind(of: StateViewController.self) }) {
            child.remove()
        }
    }
}
