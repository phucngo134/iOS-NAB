//
//  StateViewController.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit
import SnapKit

enum State {
    case loading(message: String)
    case info(title: String, message: String)
    case error(title: String, message: String)
}

class StateViewController: UIViewController {

    private let state: State
    
    init(state: State) {
        self.state = state
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        var ui: StateUI?
        switch state {
        case .loading: ui = LoadingUI()
        case .info: ui = InfoUI()
        case .error: ui = ErrorUI()
        }
        ui?.configureLayout(parentView: view)
        ui?.configureDisplay()
        ui?.state = state
    }
}
