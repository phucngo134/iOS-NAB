//
//  StateUI.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit

protocol StateUI {
    var state: State? { get set }
    func configureLayout(parentView: UIView)
    func configureDisplay()
}

private extension StateUI {
    
    func createLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontForContentSizeCategory = true
        label.textAlignment = .center
        return label
    }
    
    func createSpinner() -> UIActivityIndicatorView {
        let spinner = UIActivityIndicatorView(style: .whiteLarge)
        spinner.color = .gray
        return spinner
    }
    
    func createStackView() -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }
    
    func createImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
}

private enum Default {
    static let titleFont = UIFont.preferredFont(forTextStyle: .headline)
    static let textFont = UIFont.preferredFont(forTextStyle: .subheadline)
    static let iconHeight: CGFloat = 64.0
}

// MARK: - Loading UI (1 spinner + 1 message)
class LoadingUI: StateUI {
    private lazy var stackView: UIStackView = createStackView()
    private lazy var spinner: UIActivityIndicatorView = createSpinner()
    private lazy var subtitleLabel: UILabel = createLabel()
    
    var state: State? {
        didSet {
            guard case let .loading(message) = state else {
                fatalError("Wrong state assignment!")
            }
            subtitleLabel.attributedText = NSAttributedString(text: message, font: subtitleLabel.font, textAlignment: subtitleLabel.textAlignment)
            
            subtitleLabel.accessibilityLabel = message
        }
    }
    
    func configureLayout(parentView: UIView) {
        parentView.addSubview(stackView)
        stackView.snp.remakeConstraints { (make) in
            make.center.equalToSuperview()
            make.leading.equalToSuperview().offset(2*CGFloat.regular)
            make.trailing.equalToSuperview().offset(-2*CGFloat.regular)
        }
        stackView.addArrangedSubview(spinner)
        stackView.setCustomSpacing(.regular, after: spinner)
        stackView.addArrangedSubview(subtitleLabel)
        spinner.startAnimating()
    }
    
    func configureDisplay() {
        subtitleLabel.font = Default.textFont
    }
}

// MARK: - Info state (1 icon + 1 title + 1 subtitle)
class InfoUI: StateUI {
    private lazy var stackView: UIStackView = createStackView()
    private lazy var imageView: UIImageView = createImageView()
    private lazy var titleLabel: UILabel = createLabel()
    private lazy var subtitleLabel: UILabel = createLabel()
    
    var state: State? {
        didSet {
            guard case let .info(title, message) = state else {
                fatalError("Wrong state assignment!")
            }
            titleLabel.attributedText = NSAttributedString(text: title, font: titleLabel.font, textAlignment: titleLabel.textAlignment)
            subtitleLabel.attributedText = NSAttributedString(text: message, font: subtitleLabel.font, textAlignment: subtitleLabel.textAlignment)
            
            titleLabel.accessibilityLabel = title
            subtitleLabel.accessibilityLabel = message
        }
    }
    
    func configureLayout(parentView: UIView) {
        parentView.addSubview(stackView)
        stackView.snp.remakeConstraints { (make) in
            make.center.equalToSuperview()
            make.leading.equalToSuperview().offset(2*CGFloat.regular)
            make.trailing.equalToSuperview().offset(-2*CGFloat.regular)
        }
        stackView.addArrangedSubview(imageView)
        stackView.setCustomSpacing(.regular, after: imageView)
        imageView.snp.remakeConstraints { (make) in
            make.height.equalTo(Default.iconHeight)
        }
        stackView.addArrangedSubview(titleLabel)
        stackView.setCustomSpacing(.xSmall, after: titleLabel)
        stackView.addArrangedSubview(subtitleLabel)
    }
    
    func configureDisplay() {
        titleLabel.font = Default.titleFont
        subtitleLabel.font = Default.textFont
        imageView.image = UIImage(named: ImageName.info)
        imageView.tintColor = .blue
    }
}

// MARK: - Error state (1 icon + 1 title + 1 subtitle)
class ErrorUI: StateUI {
    private lazy var stackView: UIStackView = createStackView()
    private lazy var imageView: UIImageView = createImageView()
    private lazy var titleLabel: UILabel = createLabel()
    private lazy var subtitleLabel: UILabel = createLabel()
    
    var state: State? {
        didSet {
            guard case let .error(title, message) = state else {
                fatalError("Wrong state assignment!")
            }
            titleLabel.attributedText = NSAttributedString(text: title, font: titleLabel.font, textAlignment: titleLabel.textAlignment)
            subtitleLabel.attributedText = NSAttributedString(text: message, font: subtitleLabel.font, textAlignment: subtitleLabel.textAlignment)
            
            titleLabel.accessibilityLabel = title
            subtitleLabel.accessibilityLabel = message
        }
    }
    
    func configureLayout(parentView: UIView) {
        parentView.addSubview(stackView)
        stackView.snp.remakeConstraints { (make) in
            make.center.equalToSuperview()
            make.leading.equalToSuperview().offset(2*CGFloat.regular)
            make.trailing.equalToSuperview().offset(-2*CGFloat.regular)
        }
        stackView.addArrangedSubview(imageView)
        imageView.snp.remakeConstraints { (make) in
            make.height.equalTo(Default.iconHeight)
        }
        stackView.setCustomSpacing(.regular, after: imageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.setCustomSpacing(.xSmall, after: titleLabel)
        stackView.addArrangedSubview(subtitleLabel)
    }
    
    func configureDisplay() {
        titleLabel.font = Default.titleFont
        subtitleLabel.font = Default.textFont
        imageView.image = UIImage(named: ImageName.error)
        imageView.tintColor = .red
    }
}
