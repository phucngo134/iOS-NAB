//
//  API.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import Moya

private enum Defaults {
    static let APIKey = "60c6fbeb4b93ac653c492ba806fc346d"
}

enum OpenWeatherAPI {
    case daily16(_ parameters: Parameters)
}

extension OpenWeatherAPI {
    struct Parameters {
        let cityId: Int
        let cityName: String
        let days: Int
        let tempUnit: String
    }
}

extension OpenWeatherAPI: TargetType {
    
    var baseURL: URL {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/") else {
            fatalError("URL is not valid!")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .daily16:
            return "forecast/daily"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .daily16:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .daily16(let parameters):
            let parameters: [String: Any] = [
                "cnt": parameters.days,
                "appid": Defaults.APIKey,
                "units": parameters.tempUnit,
                "q": parameters.cityName
            ]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .daily16:
            let data = JSONParser().readLocalFile(name: "weather_forecast_mock")
            return data ?? Data()
        }
    }
    
    var headers: [String: String]? {
        return nil
    }
}
