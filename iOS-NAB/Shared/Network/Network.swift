//
//  Network.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import Moya

struct Network {
    
    @discardableResult
    static func request<T: TargetType, D: Decodable>(target: T,
                                                     completion: @escaping (Result<D, Error>) -> Void,
                                                     stub: @escaping ((MultiTarget) -> StubBehavior) = MoyaProvider.neverStub) -> Cancellable? {
        return request(provider: configureProvider(target: target, stub: stub), target: MultiTarget(target)) { (result) in
            switch result {
            case let .success(data):
                do {
                    let decoder = JSONDecoder()
                    let object = try decoder.decode(D.self, from: data)
                    completion(.success(object))
                } catch let err {
                    completion(.failure(err))
                }
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    @discardableResult
    private static func request(provider: MoyaProvider<MultiTarget>,
                                target: MultiTarget,
                                completion: @escaping (Result<Data, Error>) -> Void) -> Cancellable? {
        return provider.request(target) { (result) in
            switch result {
            case let .success(response):
                if response.containsError() {
                    completion(.failure(NetworkError.error(code: response.statusCode)))
                } else {
                    completion(.success(response.data))
                }
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    private static func configureProvider<T: TargetType>(target: TargetType,
                                                         stub: @escaping ((T) -> StubBehavior) = MoyaProvider.neverStub) -> MoyaProvider<T> {
        return MoyaProvider<T>(stubClosure: stub)
    }
}

extension Response {
    func containsError(_ range: Range<Int> = 400..<600) -> Bool {
        return range.contains(statusCode)
    }
}

enum NetworkError: Error, Equatable {
    case noConnection
    case noHeader
    
    // token
    case invalidUser
    case invalidToken
    
    // >= 400
    case badRequest
    case unauthorized
    case forbidden
    case notFound
    case notAllowed
    case timeout
    case conflict
    
    // >= 500
    case internalServerError
    case notImplemented
    case badGateway
    case serviceUnavailable
    case gatewayTimeout
    
    // other
    case other(message: String?)
    
    static func error(code: Int) -> NetworkError {
        switch code {
        case 400:
            return .badRequest
        case 401:
            return .unauthorized
        case 403:
            return .forbidden
        case 404:
            return .notFound
        case 405:
            return .notAllowed
        case 408:
            return .timeout
        case 409:
            return .conflict
            
        case 500:
            return .internalServerError
        case 501:
            return .notImplemented
        case 502:
            return .badGateway
        case 503:
            return .serviceUnavailable
        case 504:
            return .gatewayTimeout
        default:
            return .other(message: "Unknown error")
        }
    }
}

extension NetworkError {
    
    var errorDescription: String {
        switch self {
        case .noConnection:
            return "Internet connection is not available at the moment"
            
        case .invalidUser,
             .invalidToken,
             .unauthorized:
            return "Unauthorized"
            
        case .internalServerError,
             .notImplemented,
             .badGateway,
             .serviceUnavailable:
            return "Server is not available at the moment"
            
        case .gatewayTimeout:
            return "Gateway timeout"
            
        case let .other(message: message):
            return message ?? "Something went wrong. Please try again later"
        default:
            break
        }
        return "Something went wrong. Please try again later"
    }
}
