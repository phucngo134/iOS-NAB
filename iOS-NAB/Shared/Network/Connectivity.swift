//
//  Connectivity.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Alamofire

protocol Reachability {
    var isConnectedToInternet: Bool { get }
}

class Connectivity {
    private let reachabilityManager: NetworkReachabilityManager?
    
    init() {
        self.reachabilityManager = NetworkReachabilityManager()
    }
}

extension Connectivity: Reachability {
    
    var isConnectedToInternet: Bool {
        return reachabilityManager?.isReachable == true
    }
}
