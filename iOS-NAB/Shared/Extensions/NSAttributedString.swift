//
//  NSAttributedString.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit

extension NSAttributedString {
    
    convenience init(text: String, font: UIFont, lineSpacing: CGFloat = 2.0, textAlignment: NSTextAlignment = .natural) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = textAlignment
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.hyphenationFactor = 1.0
        
        let attributedString = NSMutableAttributedString(string: text)
        let range = NSRange(location: 0, length: attributedString.length)
        attributedString.addAttribute(.font, value: font, range: range)
        attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: range)
        self.init(attributedString: attributedString)
    }
}
