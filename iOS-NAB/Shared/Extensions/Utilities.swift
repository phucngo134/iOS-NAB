//
//  Utilities.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit

func print(_ items: Any...) {
    #if DEBUG
        guard items.count > 0 else { return }
        Swift.debugPrint(items[0])
    #endif
}

extension Result where Success == Void {
    static var success: Result {
        return .success(())
    }
}

extension Data {
    // Print on console without "\" character
    var consoleJSON: NSString {
        return NSString(string: prettyJSON)
    }
    
    var prettyJSON: String {
        guard let jsonObject = try? JSONSerialization.jsonObject(with: self, options: []) else {
            return "Data is not a valid json"
        }
        if let dict = jsonObject as? NSDictionary {
            let swiftDict = dict.toSwiftDict()
            return swiftDict.prettyJSON
        } else if let array = jsonObject as? [NSDictionary] {
            var result = "[\n"
            result += array.map { $0.toSwiftDict().prettyJSON }
                .joined(separator: ",")
            result += "]"
            return result
        }
        return "{}"
    }
}

extension Dictionary {
    var prettyJSON: String {
        if let theJSONData = try? JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted]),
            let theJSONText = String(data: theJSONData, encoding: String.Encoding.utf8) {
            return theJSONText
        }
        return "{}"
    }
}

extension NSDictionary {
    func toSwiftDict() -> [String: Any] {
        var swiftDict: [String: Any] = [:]
        for key in allKeys {
            let stringKey = key as? String
            if let key = stringKey, let keyValue = value(forKey: key) {
                swiftDict[key] = keyValue
            }
        }
        return swiftDict
    }
}

extension DateFormatter {
    static let unixDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let abbreviation = TimeZone.current.abbreviation() ?? "UTC"
        formatter.timeZone = TimeZone(abbreviation: abbreviation)
        formatter.locale = Locale.current
        formatter.dateFormat = "E, d MMM yyyy"
        return formatter
    }()
    
    static let fullDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let abbreviation = TimeZone.current.abbreviation() ?? "UTC"
        formatter.timeZone = TimeZone(abbreviation: abbreviation)
        formatter.locale = Locale.current
        formatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
        return formatter
    }()
}

extension UIViewController {
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }

    func remove() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}
