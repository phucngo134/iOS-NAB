//
//  CGFloat.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import CoreGraphics

extension CGFloat {
    static let xSmall: CGFloat = 4.0
    static let tiny: CGFloat = 8.0
    static let small: CGFloat = 12.0
    static let regular: CGFloat = 16.0
}
