//
//  JSONParser.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

class JSONParser {
    private let bundle: Bundle
    
    init(bundle: Bundle = .main) {
        self.bundle = bundle
    }
    
    func readLocalFile(name: String) -> Data? {
        do {
            if let bundlePath = bundle.path(forResource: name, ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf16) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }
    
    func parse<T: Decodable>(name: String) -> [T]? {
        do {
            if let jsonData = readLocalFile(name: name) {
                let decodedData = try JSONDecoder().decode([T].self, from: jsonData)
                return decodedData
            }
        } catch {
            print("decode error: \(error)")
        }
        return nil
    }
}
