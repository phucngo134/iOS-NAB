//
//  Constants.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

enum Accessibility {
    
    enum Label {
        static let city = "City"
        static let country = "Country"
        static let history = "History"
        static let result = "Result"
        static let selected = "Selected"
        static let unselected = "Unselected"
        static let days = "Days of forecast"
        static let tempUnit = "Temperature unit"
    }
}

enum StateText {
    static let noCityTitle = "No city found"
    static let noCityMessage = "Please make sure you input a correct city name."
    static let loading = "Loading data..."
    static let errorTitle = "An error happened"
}

enum Text {
    static let cityPlaceholder = "Enter a city name (minimum 3 letters)..."
    static let days = "Days:"
    static let tempUnit = "Unit:"
}

enum ErrorText {
    static let general = "Something went wrong.\nPlease try again later!"
}

enum NavigationTitle {
    static let weather = "Weather"
    static let city = "City"
    static let days = "Days of Forecast"
    static let tempUnit = "Temperature Unit"
}

enum ButtonTitle {
    static let reset = "Reset"
}

enum ImageName {
    static let info = "Info"
    static let error = "Error"
    static let search = "Search"
    static let history = "History"
}
