//
//  AppDelegate.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 15/04/2021.
//

import UIKit
import RealmSwift

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.absoluteString ?? ""
        print("Document dir: \(documentsURL)")
        
        // Load the masterdata from a json file
        let realmStorage = StorageProvider().makeStorage()
        if let cities: [City] = JSONParser().parse(name: "world-cities") {
            try? realmStorage.save(objects: cities)
        }

        self.window = UIWindow(frame: UIScreen.main.bounds)
        let weatherModule = WeatherModule()
        self.window?.rootViewController = UINavigationController(rootViewController: weatherModule.viewController)
        self.window?.makeKeyAndVisible()
        
        return true
    }
}
