//
//  WeatherForecast.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import RealmSwift

enum WeatherForecastColumn: String {
    case id
    case days
    case tempUnit
    case createdAt
}

class WeatherForecast: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var days: Int = 0
    @objc dynamic var tempUnit: String = ""
    @objc dynamic var createdAt = Date(timeIntervalSince1970: 1)
    let forecastData = List<DailyForecast>()
    
    convenience init(id: Int, days: Int, tempUnit: String, createAt: Date, forecastData: [DailyForecast]) {
        self.init()
        self.id = id
        self.days = days
        self.tempUnit = tempUnit
        self.createdAt = createAt
        self.forecastData.removeAll()
        forecastData.forEach { self.forecastData.append($0) }
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? WeatherForecast else { return false }
        let dateFormatter = DateFormatter.fullDateFormatter
        return id == rhs.id
            && days == rhs.days
            && tempUnit == rhs.tempUnit
            && dateFormatter.string(from: createdAt) == dateFormatter.string(from: rhs.createdAt)
            && forecastData == rhs.forecastData
    }
}
