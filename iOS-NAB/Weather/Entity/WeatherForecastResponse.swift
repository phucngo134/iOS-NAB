//
//  WeatherForecastResponse.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

struct WeatherForecastResponse: Decodable {
    let list: [Daily]
    
    private enum CodingKeys: String, CodingKey {
        case list
    }
}

extension WeatherForecastResponse {
    
    struct Daily: Decodable {
        let timestamp: Double
        let pressure: Float
        let humidity: Float
        let temp: Temperature
        let weather: [Weather]
        
        private enum CodingKeys: String, CodingKey {
            case timestamp = "dt"
            case pressure = "pressure"
            case humidity = "humidity"
            case temp = "temp"
            case weather = "weather"
        }
    }
    
    struct Temperature: Decodable {
        let min: Float
        let max: Float
        
        private enum CodingKeys: String, CodingKey {
            case min
            case max
        }
    }
    
    struct Weather: Decodable {
        let descr: String
        
        private enum CodingKeys: String, CodingKey {
            case descr = "description"
        }
    }
}
