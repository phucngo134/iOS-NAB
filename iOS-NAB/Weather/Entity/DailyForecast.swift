//
//  DailyForecast.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 17/04/2021.
//

import Foundation
import RealmSwift

class DailyForecast: Object {
    @objc dynamic var date = Date(timeIntervalSince1970: 1)
    @objc dynamic var averageTemp: String = ""
    @objc dynamic var pressure: String = ""
    @objc dynamic var humidity: String = ""
    @objc dynamic var evaluation: String = ""
    
    convenience init(response: WeatherForecastResponse.Daily) {
        self.init()
        date = Date(timeIntervalSince1970: response.timestamp)
        averageTemp = String(format: "%.0f", (response.temp.min + response.temp.max) / 2.0)
        humidity = String(format: "%.0f", response.humidity)
        pressure = String(format: "%.0f", response.pressure)
        evaluation = response.weather.first?.descr ?? ""
    }
    
    convenience init(date: Date, averageTemp: String, pressure: String, humidity: String, evaluation: String) {
        self.init()
        self.date = date
        self.averageTemp = averageTemp
        self.pressure = pressure
        self.humidity = humidity
        self.evaluation = evaluation
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? DailyForecast else { return false }
        let dateFormatter = DateFormatter.fullDateFormatter
        return dateFormatter.string(from: date) == dateFormatter.string(from: rhs.date)
            && averageTemp == rhs.averageTemp
            && pressure == rhs.pressure
            && humidity == rhs.humidity
            && evaluation == rhs.evaluation
    }
}
