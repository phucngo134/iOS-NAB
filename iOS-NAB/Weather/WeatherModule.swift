//
//  WeatherModule.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation

class WeatherModule {
    private(set) var viewController: WeatherViewProtocol
    
    init() {
        let connectivity = Connectivity()
        let settings = UserDefaultSettings()
        let storage = StorageProvider().makeStorage()
        let interactor = WeatherForecastInteractor(connectivity: connectivity, settings: settings, storage: storage)
        let router = WeatherRouter()
        let presenter = WeatherForecastPresenter(interactor: interactor, router: router)
        let viewController = WeatherViewController(presenter: presenter)
        presenter.view = viewController
        self.viewController = viewController
        router.viewController = viewController
    }
}
