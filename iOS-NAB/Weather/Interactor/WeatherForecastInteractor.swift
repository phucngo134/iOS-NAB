//
//  WeatherForecastInteractor.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import Moya

protocol WeatherForecastInteraction {
    var lastSelectedDays: Int { get }
    var lastSelectedTempUnit: TempUnit { get }
    func resetConfig()
    func shouldFetchWeatherForecast(parameters: OpenWeatherAPI.Parameters) -> Bool
    func getLocalWeatherForecast(parameters: OpenWeatherAPI.Parameters) -> [DailyForecastViewModel]
    func fetchWeatherForecast(parameters: OpenWeatherAPI.Parameters,
                              isMocked: Bool,
                              completion: @escaping (Result<[DailyForecastViewModel], Error>) -> Void)
}

class WeatherForecastInteractor: WeatherForecastInteraction {
    
    enum Defaults {
        static let days: Int = 7
        static let tempUnit: TempUnit = .standard
        static let livingTime: Int = 5 // 5 minutes
    }
    
    private let connectivity: Reachability
    private let settings: IntSettingsStoring & StringSettingsStoring
    private let storage: StorageContext
    
    init(connectivity: Reachability, settings: IntSettingsStoring & StringSettingsStoring, storage: StorageContext) {
        self.connectivity = connectivity
        self.settings = settings
        self.storage = storage
    }
    
    var lastSelectedDays: Int {
        let selectedDays = settings.loadInt(key: .days)
        guard selectedDays > 0 else {
            return Defaults.days
        }
        return selectedDays
    }
    
    var lastSelectedTempUnit: TempUnit {
        guard let selectedTempUnit = settings.loadString(key: .tempUnit) else {
            return Defaults.tempUnit
        }
        return TempUnit.allCases.first(where: { $0.rawValue == selectedTempUnit }) ?? Defaults.tempUnit
    }
    
    func resetConfig() {
        settings.remove(key: .days)
        settings.remove(key: .tempUnit)
    }
    
    func shouldFetchWeatherForecast(parameters: OpenWeatherAPI.Parameters) -> Bool {
        if let savedData = getWeatherForecastFromDatabase(parameters: parameters) {
            let currentDate = Date()
            let minutes = Calendar.current.dateComponents([.minute], from: savedData.createdAt, to: currentDate).minute ?? 0
            return minutes > Defaults.livingTime
        }
        return true
    }
    
    func getLocalWeatherForecast(parameters: OpenWeatherAPI.Parameters) -> [DailyForecastViewModel] {
        if let savedData = getWeatherForecastFromDatabase(parameters: parameters) {
            var viewModels: [DailyForecastViewModel] = []
            let tempUnit = savedData.tempUnit
            savedData.forecastData.forEach { viewModels.append(DailyForecastViewModel(model: $0, tempUnit: tempUnit)) }
            return viewModels
        }
        return []
    }
    
    // "isMocked = true" only used for unit testings to use mocked data for network request
    // for main app, it should be "isMocked = false"
    func fetchWeatherForecast(parameters: OpenWeatherAPI.Parameters,
                              isMocked: Bool,
                              completion: @escaping (Result<[DailyForecastViewModel], Error>) -> Void) {
        // Check if there is an available internet connection first
        if !connectivity.isConnectedToInternet {
            completion(.failure(NetworkError.noConnection))
            return
        }
        // ... then call the API
        let target = OpenWeatherAPI.daily16(parameters)
        Network.request(target: target) { [weak self] (result: Result<WeatherForecastResponse, Error>) in
            guard let self = self else { return }
            switch result {
            case let .success(response):
                let daysForecast = response.list.map { DailyForecast(response: $0) }
                // Save the data into database
                self.saveWeatherForecastIntoDatabase(parameters: parameters, forecastData: daysForecast)
                
                let viewModels = daysForecast.map { DailyForecastViewModel(model: $0, tempUnit: parameters.tempUnit) }
                completion(.success(viewModels))
                
            case let .failure(error):
                completion(.failure(error))
            }
        } stub: { _ -> StubBehavior in
            guard !isMocked else {
                return .immediate
            }
            return .never
        }
    }
}

extension WeatherForecastInteractor {
    
    private func getWeatherForecastFromDatabase(parameters: OpenWeatherAPI.Parameters) -> WeatherForecast? {
        let idColumn = WeatherForecastColumn.id.rawValue
        let daysColumn = WeatherForecastColumn.days.rawValue
        let tempUnitColumn = WeatherForecastColumn.tempUnit.rawValue
        
        let predicate = NSPredicate(format: "\(idColumn) = %d && \(daysColumn) = %d && \(tempUnitColumn) = %@", parameters.cityId, parameters.days, parameters.tempUnit)
        
        return storage.fetch(WeatherForecast.self, predicate: predicate).first
    }
    
    private func saveWeatherForecastIntoDatabase(parameters: OpenWeatherAPI.Parameters, forecastData: [DailyForecast]) {
        if let savedData = getWeatherForecastFromDatabase(parameters: parameters) {
            try? storage.update {
                savedData.createdAt = Date()
                savedData.forecastData.removeAll()
                forecastData.forEach { savedData.forecastData.append($0) }
            }
        } else {
            let weatherForecast = WeatherForecast(id: parameters.cityId,
                                                  days: parameters.days,
                                                  tempUnit: parameters.tempUnit,
                                                  createAt: Date(),
                                                  forecastData: forecastData)
            try? storage.save(object: weatherForecast)
        }
    }
}
