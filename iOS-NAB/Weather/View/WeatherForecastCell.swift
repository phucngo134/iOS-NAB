//
//  WeatherForecastCell.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit
import SnapKit

protocol WeatherForecastCellDisplay {
    static var reuseIdentifier: String { get }
    func setupWeatherForecast(_ viewModel: DailyForecastViewModel)
}

class WeatherForecastCell: UITableViewCell {

    private enum Defaults {
        static let titleFont = UIFont.preferredFont(forTextStyle: .body)
        static let prefixDate = "Date: "
        static let prefixTemp = "Average Temperature: "
        static let prefixPressure = "Pressure: "
        static let prefixHumidity = "Humidity: "
        static let prefixDescr = "Description: "
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureLayout()
        configureDisplay()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var contentLabel: UILabel = createLabel()
}

private extension WeatherForecastCell {
    
    func configureLayout() {
        contentView.addSubview(contentLabel)
        
        contentLabel.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().offset(CGFloat.regular)
            make.trailing.bottom.equalToSuperview().offset(-CGFloat.regular)
        }
    }
    
    func configureDisplay() {
        selectionStyle = .none
        contentLabel.font = Defaults.titleFont
    }
    
    func createLabel() -> UILabel {
        let label = UILabel()
        label.adjustsFontForContentSizeCategory = true
        label.isAccessibilityElement = true
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        return label
    }
}

extension WeatherForecastCell: WeatherForecastCellDisplay {
    
    static var reuseIdentifier: String {
        return "WeatherForecastCell"
    }
    
    func setupWeatherForecast(_ viewModel: DailyForecastViewModel) {
        let content = accessibilityContentFor(viewModel)
        contentLabel.attributedText = NSAttributedString(text: content, font: contentLabel.font)
        accessibilityLabel = content
    }
}

private extension WeatherForecastCell {
    
    func accessibilityContentFor(_ viewModel: DailyForecastViewModel) -> String {
        var content: [String] = []
        
        let dateText = Defaults.prefixDate + viewModel.date
        content.append(dateText)
        
        let tempText = Defaults.prefixTemp + viewModel.averageTemp
        content.append(tempText)
        
        let pressureText = Defaults.prefixPressure + viewModel.pressure
        content.append(pressureText)
        
        let humidityText = Defaults.prefixHumidity + viewModel.humidity
        content.append(humidityText)
        
        let descrText = Defaults.prefixDescr + viewModel.evaluation
        content.append(descrText)
        
        return content.joined(separator: "\n")
    }
}
