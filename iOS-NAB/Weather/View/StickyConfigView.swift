//
//  StickyConfigView.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit

protocol StickyConfigDisplay {
    var onDaysTap: (() -> Void)? { get set }
    var onUnitTap: (() -> Void)? { get set }
    func display(selectedDays days: Int, selectedTempUnit tempUnit: TempUnit)
}

class StickyConfigView: UIView, StickyConfigDisplay {
    
    enum Defaults {
        static let textFont = UIFont.preferredFont(forTextStyle: .body)
        static let valueFont = UIFont.preferredFont(forTextStyle: .headline)
    }
    
    private lazy var daysTextLabel = createLabel()
    private lazy var daysValueButton = createButton()
    private lazy var divider = UILabel()
    private lazy var unitTextLabel = createLabel()
    private lazy var unitValueButton = createButton()
    
    var onDaysTap: (() -> Void)?
    var onUnitTap: (() -> Void)?
    
    func display(selectedDays days: Int, selectedTempUnit tempUnit: TempUnit) {
        let daysAttributedText = NSAttributedString(text: "\(days)", font: Defaults.valueFont)
        daysValueButton.setAttributedTitle(daysAttributedText, for: .normal)
        
        let tempUnitAttributedText = NSAttributedString(text: tempUnit.longTextDisplay, font: Defaults.valueFont)
        unitValueButton.setAttributedTitle(tempUnitAttributedText, for: .normal)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureLayout()
        configureDisplay()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureLayout() {
        addSubview(daysTextLabel)
        addSubview(daysValueButton)
        addSubview(divider)
        addSubview(unitTextLabel)
        addSubview(unitValueButton)
        
        divider.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.5)
            make.width.equalTo(1.0)
        }
        daysTextLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(CGFloat.regular)
            make.top.equalToSuperview().offset(CGFloat.tiny)
            make.bottom.equalToSuperview().offset(-CGFloat.tiny)
            make.trailing.equalTo(daysValueButton.snp.leading)
        }
        daysValueButton.snp.makeConstraints { make in
            make.trailing.equalTo(divider.snp.leading).offset(-CGFloat.regular)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.25)
        }
        unitTextLabel.snp.makeConstraints { make in
            make.leading.equalTo(divider.snp.trailing).offset(CGFloat.regular)
            make.trailing.equalTo(unitValueButton.snp.leading)
            make.centerY.equalToSuperview()
        }
        unitValueButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-CGFloat.regular)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.25)
        }
    }
            
    @objc private func daysValueLabelDidTap() {
        onDaysTap?()
    }
    
    @objc private func unitValueLabelDidTap() {
        onUnitTap?()
    }
    
    private func configureDisplay() {
        divider.backgroundColor = .lightGray
        daysTextLabel.font = Defaults.textFont
        daysTextLabel.attributedText = NSAttributedString(text: Text.days, font: daysTextLabel.font)
        unitTextLabel.font = Defaults.textFont
        unitTextLabel.attributedText = NSAttributedString(text: Text.tempUnit, font: unitTextLabel.font)
        
        daysValueButton.addTarget(self, action: #selector(daysValueLabelDidTap), for: .touchUpInside)
        daysValueButton.accessibilityLabel = Accessibility.Label.days
        
        unitValueButton.addTarget(self, action: #selector(unitValueLabelDidTap), for: .touchUpInside)
        unitValueButton.accessibilityLabel = Accessibility.Label.tempUnit
    }
    
    private func createLabel() -> UILabel {
        let label = UILabel()
        label.adjustsFontForContentSizeCategory = true
        return label
    }
    
    private func createButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.contentHorizontalAlignment = .center
        button.titleLabel?.adjustsFontForContentSizeCategory = true
        button.titleLabel?.lineBreakMode = .byTruncatingTail
        return button
    }
}
