//
//  DailyForecastViewModel.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import DifferenceKit

struct DailyForecastViewModel {
    let date: String
    let averageTemp: String
    let pressure: String
    let humidity: String
    let evaluation: String
}

extension DailyForecastViewModel {
    
    init(model: DailyForecast, tempUnit: String) {
        date = DateFormatter.unixDateFormatter.string(from: model.date)
        
        var symbol = ""
        if let unit = TempUnit.allCases.first(where: { $0.rawValue == tempUnit }) {
            symbol = unit.shortTextDisplay
        }
        averageTemp = model.averageTemp + symbol
        humidity = model.humidity + "%"
        pressure = model.pressure
        evaluation = model.evaluation
    }
}

extension DailyForecastViewModel: Equatable {
    static func == (lhs: DailyForecastViewModel, rhs: DailyForecastViewModel) -> Bool {
        return lhs.isContentEqual(to: rhs)
    }
}

extension DailyForecastViewModel: Differentiable {
    
    var differenceIdentifier: String {
        return date
    }
    
    func isContentEqual(to source: DailyForecastViewModel) -> Bool {
        return date == source.date
            && averageTemp == source.averageTemp
            && pressure == source.pressure
            && humidity == source.humidity
            && evaluation == source.evaluation
    }
}
