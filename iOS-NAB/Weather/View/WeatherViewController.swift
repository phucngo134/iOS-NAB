//
//  WeatherViewController.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit
import SnapKit
import DifferenceKit

protocol WeatherViewProtocol: UIViewController {
    func displayLoadingState()
    func displayErrorState(errorMessage: String)
    func hideAnyState()
    func reload(changes: StagedChangeset<[DailyForecastViewModel]>)
}

class WeatherViewController: UIViewController, StateDisplay {
    
    private let presenter: WeatherForecastPresentation
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private lazy var stickyConfigView = StickyConfigView()
    
    init(presenter: WeatherForecastPresentation) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLayout()
        configureDisplay()
        configureSearchController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.displayStickyConfigView(stickyConfigView)
    }
}

// MARK: - UITableViewDataSource
extension WeatherViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.totalItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: WeatherForecastCell.reuseIdentifier, for: indexPath) as? WeatherForecastCell {
            presenter.displayWeatherForecast(cell, atIndex: indexPath.row)
            return cell
        }
        fatalError("Not found cell!")
    }
}

// MARK: - UITableViewDelegate
extension WeatherViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
}

// MARK: - WeatherViewProtocol
extension WeatherViewController: WeatherViewProtocol {
    
    func displayLoadingState() {
        let state = State.loading(message: StateText.loading)
        displayState(state)
    }
    
    func displayErrorState(errorMessage: String) {
        let state = State.error(title: StateText.errorTitle, message: errorMessage)
        displayState(state)
    }
    
    func hideAnyState() {
        hideState()
    }
    
    func reload(changes: StagedChangeset<[DailyForecastViewModel]>) {
        tableView.reload(using: changes, with: .none) { [weak self] data in
            self?.presenter.syncData(data)
            self?.hideState()
        }
    }
}

// MARK: - UISearchResultsUpdating
extension WeatherViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        if #available(*, iOS 12.0) { // Always show searchResultsController
            searchController.searchResultsController?.view.isHidden = false
        }
    }
}

private extension WeatherViewController {
    
    func configureDisplay() {
        view.backgroundColor = .white
        navigationItem.title = NavigationTitle.weather
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: ButtonTitle.reset, style: .plain, target: self, action: #selector(reset))
        
        tableView.register(WeatherForecastCell.self, forCellReuseIdentifier: WeatherForecastCell.reuseIdentifier)
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
    }
    
    func configureLayout() {
        view.addSubview(stickyConfigView)
        view.addSubview(tableView)
        
        stickyConfigView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.trailing.equalToSuperview()
        }
        tableView.snp.makeConstraints { make in
            make.top.equalTo(stickyConfigView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
        stickyConfigView.onDaysTap = { [weak self] in
            self?.presenter.openNumberOfDaysForecastOptions()
        }
        stickyConfigView.onUnitTap = { [weak self] in
            self?.presenter.openTemperatureUnitOptions()
        }
    }
    
    func configureSearchController() {
        
        let cityModule = CityModule()
        let searchResultsController = cityModule.viewController
        searchResultsController.onSelectCity = { [unowned self] selectedCity in
            self.navigationItem.searchController?.isActive = false
            self.navigationItem.searchController?.searchBar.text = selectedCity.name
            self.presenter.displayWeatherForecast(selectedCity)
        }
        let searchController = UISearchController(searchResultsController: searchResultsController)
        if #available(iOS 13.0, *) {
            searchController.showsSearchResultsController = true
        } else {
            searchController.searchResultsUpdater = self
        }
        searchController.obscuresBackgroundDuringPresentation = true
        searchController.searchBar.delegate = searchResultsController
        searchController.searchBar.placeholder = Text.cityPlaceholder
        definesPresentationContext = true
        navigationItem.searchController = searchController
    }
    
    @objc func reset() {
        navigationItem.searchController?.searchBar.text = nil
        presenter.resetConfig()
        presenter.displayStickyConfigView(stickyConfigView)
        presenter.displayWeatherForecast(nil)
    }
}
