//
//  WeatherRouter.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import UIKit

typealias OnChangeDataAction = () -> Void

protocol WeatherRoutes {
    func openNumberOfDaysForecastOptions(onChangeData: OnChangeDataAction?)
    func openTemperatureUnitOptions(onChangeData: OnChangeDataAction?)
}

final class WeatherRouter: RouterProtocol, WeatherRoutes {
    weak var viewController: UIViewController?
    
    func openNumberOfDaysForecastOptions(onChangeData: OnChangeDataAction?) {
        let module = DayOptionsModule()
        module.viewController.onChangeData = onChangeData
        viewController?.navigationController?.pushViewController(module.viewController, animated: true)
    }
    
    func openTemperatureUnitOptions(onChangeData: OnChangeDataAction?) {
        let module = UnitOptionsModule()
        module.viewController.onChangeData = onChangeData
        viewController?.navigationController?.pushViewController(module.viewController, animated: true)
    }
}
