//
//  WeatherForecastPresenter.swift
//  iOS-NAB
//
//  Created by Phuc Ngo on 16/04/2021.
//

import Foundation
import DifferenceKit

protocol WeatherForecastPresentation {
    var totalItems: Int { get }
    func syncData(_ data: [DailyForecastViewModel])
    func displayWeatherForecast(_ cellDisplay: WeatherForecastCellDisplay, atIndex index: Int)
    func displayWeatherForecast(_ city: CityViewModel?)
    func displayStickyConfigView(_ view: StickyConfigDisplay)
    func resetConfig()
    func openNumberOfDaysForecastOptions()
    func openTemperatureUnitOptions()
}

class WeatherForecastPresenter {
    private let interactor: WeatherForecastInteraction
    private let router: WeatherRoutes
    private var lastParameters: OpenWeatherAPI.Parameters?
    
    private var data: [DailyForecastViewModel] = []
    weak var view: WeatherViewProtocol?
    
    init(interactor: WeatherForecastInteraction, router: WeatherRoutes) {
        self.interactor = interactor
        self.router = router
    }
}

extension WeatherForecastPresenter: WeatherForecastPresentation {
    var totalItems: Int {
        return data.count
    }
    
    func syncData(_ data: [DailyForecastViewModel]) {
        self.data = data
    }
    
    func displayWeatherForecast(_ cellDisplay: WeatherForecastCellDisplay, atIndex index: Int) {
        guard data.indices.contains(index) else { return }
        let weatherForecast = data[index]
        cellDisplay.setupWeatherForecast(weatherForecast)
    }
    
    func displayWeatherForecast(_ city: CityViewModel?) {
        
        // Display empty list
        guard let city = city else {
            updateChanges([])
            return
        }
        // Ready to make a request
        view?.displayLoadingState()
        let parameters = OpenWeatherAPI.Parameters(cityId: city.id,
                                               cityName: city.name,
                                               days: interactor.lastSelectedDays,
                                               tempUnit: interactor.lastSelectedTempUnit.rawValue)
        fetchWeatherForecast(parameters)
        
    }
    
    func displayStickyConfigView(_ view: StickyConfigDisplay) {
        view.display(selectedDays: interactor.lastSelectedDays,
                     selectedTempUnit: interactor.lastSelectedTempUnit)
    }
    
    func resetConfig() {
        interactor.resetConfig()
        lastParameters = nil
    }
    
    func openNumberOfDaysForecastOptions() {
        router.openNumberOfDaysForecastOptions(onChangeData: { [unowned self] in
            guard let parameters = self.createParameters() else { return }
            self.fetchWeatherForecast(parameters)
        })
    }
    
    func openTemperatureUnitOptions() {
        router.openTemperatureUnitOptions(onChangeData: { [unowned self] in
            guard let parameters = self.createParameters() else { return }
            self.fetchWeatherForecast(parameters)
        })
    }
}

private extension WeatherForecastPresenter {
    
    func updateChanges(_ newData: [DailyForecastViewModel]) {
        let changes = StagedChangeset<[DailyForecastViewModel]>(source: data, target: newData)
        if changes.isEmpty {
            view?.hideAnyState()
        } else {
            view?.reload(changes: changes)
        }
    }
    
    func createParameters() -> OpenWeatherAPI.Parameters? {
        guard let lastParameters = lastParameters else { return nil }
        let parameters = OpenWeatherAPI.Parameters(cityId: lastParameters.cityId,
                                                   cityName: lastParameters.cityName,
                                                   days: interactor.lastSelectedDays,
                                                   tempUnit: interactor.lastSelectedTempUnit.rawValue)
        return parameters
    }
    
    func fetchWeatherForecast(_ parameters: OpenWeatherAPI.Parameters) {
        // Check if it is necessary to fetch new data from API
        let shouldFetchWeatherForecast = interactor.shouldFetchWeatherForecast(parameters: parameters)
        
        guard shouldFetchWeatherForecast else {
            // Save the last search parameters
            lastParameters = parameters
            // Dispaly local saved data
            let localData = interactor.getLocalWeatherForecast(parameters: parameters)
            updateChanges(localData)
            return
        }
        // Fetch the new data from API
        interactor.fetchWeatherForecast(parameters: parameters, isMocked: false) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(viewModels):
                self.updateChanges(viewModels)
            case let .failure(error):
                if let networkError = error as? NetworkError {
                    self.view?.displayErrorState(errorMessage: networkError.errorDescription)
                } else {
                    self.view?.displayErrorState(errorMessage: ErrorText.general)
                }
            }
        }
        // Save the last search parameters
        lastParameters = parameters
    }
}
