platform :ios, '12.0'

install! 'cocoapods',
# Generate a different .xcproj file for each pod, which in itself doesn't mean much but affect the below option
:generate_multiple_pod_projects => false,

# greatly improve install speed if not many pods are changed. Can only be used if pod is separated in its own .xcodeproj
:incremental_installation => false,

# Allow you to modify pod source code, which is undesirable, but greatly improve pod install speed which is sweet for CI.
:lock_pod_sources => false,

# Disable input output path for cocoapods copy script phase
:disable_input_output_paths => true,

:warn_for_unused_master_specs_repo => false

def share_pods
  pod 'RealmSwift', '~> 10.7.2'
  pod 'SwiftLint', '~> 0.43.1'
  pod 'SnapKit', '~> 5.0.1'
  pod 'DifferenceKit', '~> 1.1.5'
  pod 'Alamofire', '~> 5.4.1'
  pod 'Moya', '~> 14.0.0'
end

target 'iOS-NAB' do
  # Comment the next line if you don't want to use dynamic frameworks
  use_frameworks!

  # Pods for iOS-NAB
  share_pods

  target 'iOS-NABTests' do
    inherit! :search_paths
    # Pods for testing
  end

  target 'iOS-NABUITests' do
    # Pods for testing
  end

end

post_install do |installer|
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      if config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'].to_f < 12.0
        config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '12.0'
      end
    end
  end
end
